Plugin
======

A Symfony project created on March 16, 2018, 12:33 pm.

Description : 

 - Developement d'un module pour l'extrcation des Tickets sur Redmine , Pour
 
 génerer des Raports et des statistiques concernant les tickets d'un Projet ou des tickets
 
 assosciées à un utilisateur .
 
Pré-requis : 

 1 - Redmine 
 2 - Activation de L'API REST et JSONP sur Redmine .

Plugin Developé à l'aide de Symfony 3.0.9 
