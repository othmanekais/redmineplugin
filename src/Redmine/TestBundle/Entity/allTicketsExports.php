<?php

namespace Redmine\TestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * allTicketsExports
 *
 * @ORM\Table(name="all_tickets_exports")
 * @ORM\Entity(repositoryClass="Redmine\TestBundle\Repository\allTicketsExportsRepository")
 */
class allTicketsExports
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Nom_Projet", type="string", length=255)
     */
    private $nomProjet;

    /**
     * @var string
     *
     * @ORM\Column(name="Ticket", type="string", length=255)
     */
    private $ticket;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_debut_plannifie", type="datetime",nullable = true)
     */
    private $dateDebutPlannifie;

    /**
     * @var int
     *
     * @ORM\Column(name="temps_estime", type="integer",nullable = true)
     */
    private $tempsEstime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_fin_plannifiee", type="datetime" , nullable = true)
     */
    private $dateFinPlannifiee;

    /**
     * @var int
     *
     * @ORM\Column(name="Avancement", type="integer" , nullable=true)
     */
    private $avancement;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_debut_reelle", type="datetime",nullable=true)
     */
    private $dateDebutReelle;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_fin_reelle", type="datetime",nullable=true)
     */
    private $dateFinReelle;

    /**
     * @var int
     *
     * @ORM\Column(name="Total_temps_passe", type="integer",nullable=true)
     */
    private $totalTempsPasse;

    /**
     * @var int
     *
     * @ORM\Column(name="Total_temps_passe_mois", type="integer",nullable=true)
     */
    private $totalTempsPasseMois;

    /**
     * @var int
     *
     * @ORM\Column(name="depacement", type="integer",nullable=true)
     */
    private $depacement;

    /**
     * @var int
     *
     * @ORM\Column(name="Respect_date_debut", type="integer",nullable=true)
     */
    private $respectDateDebut;

    /**
     * @var int
     *
     * @ORM\Column(name="Respect_date_fin", type="integer",nullable=true)
     */
    private $respectDateFin;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nomProjet
     *
     * @param string $nomProjet
     *
     * @return allTicketsExports
     */
    public function setNomProjet($nomProjet)
    {
        $this->nomProjet = $nomProjet;

        return $this;
    }

    /**
     * Get nomProjet
     *
     * @return string
     */
    public function getNomProjet()
    {
        return $this->nomProjet;
    }

    /**
     * Set ticket
     *
     * @param string $ticket
     *
     * @return allTicketsExports
     */
    public function setTicket($ticket)
    {
        $this->ticket = $ticket;

        return $this;
    }

    /**
     * Get ticket
     *
     * @return string
     */
    public function getTicket()
    {
        return $this->ticket;
    }

    /**
     * Set dateDebutPlannifie
     *
     * @param \DateTime $dateDebutPlannifie
     *
     * @return allTicketsExports
     */
    public function setDateDebutPlannifie($dateDebutPlannifie)
    {
        $this->dateDebutPlannifie = $dateDebutPlannifie;

        return $this;
    }

    /**
     * Get dateDebutPlannifie
     *
     * @return \DateTime
     */
    public function getDateDebutPlannifie()
    {
        return $this->dateDebutPlannifie;
    }

    /**
     * Set tempsEstime
     *
     * @param integer $tempsEstime
     *
     * @return allTicketsExports
     */
    public function setTempsEstime($tempsEstime)
    {
        $this->tempsEstime = $tempsEstime;

        return $this;
    }

    /**
     * Get tempsEstime
     *
     * @return int
     */
    public function getTempsEstime()
    {
        return $this->tempsEstime;
    }

    /**
     * Set dateFinPlannifiee
     *
     * @param \DateTime $dateFinPlannifiee
     *
     * @return allTicketsExports
     */
    public function setDateFinPlannifiee($dateFinPlannifiee)
    {
        $this->dateFinPlannifiee = $dateFinPlannifiee;

        return $this;
    }

    /**
     * Get dateFinPlannifiee
     *
     * @return \DateTime
     */
    public function getDateFinPlannifiee()
    {
        return $this->dateFinPlannifiee;
    }

    /**
     * Set avancement
     *
     * @param integer $avancement
     *
     * @return allTicketsExports
     */
    public function setAvancement($avancement)
    {
        $this->avancement = $avancement;

        return $this;
    }

    /**
     * Get avancement
     *
     * @return int
     */
    public function getAvancement()
    {
        return $this->avancement;
    }

    /**
     * Set dateDebutReelle
     *
     * @param \DateTime $dateDebutReelle
     *
     * @return allTicketsExports
     */
    public function setDateDebutReelle($dateDebutReelle)
    {
        $this->dateDebutReelle = $dateDebutReelle;

        return $this;
    }

    /**
     * Get dateDebutReelle
     *
     * @return \DateTime
     */
    public function getDateDebutReelle()
    {
        return $this->dateDebutReelle;
    }

    /**
     * Set dateFinReelle
     *
     * @param \DateTime $dateFinReelle
     *
     * @return allTicketsExports
     */
    public function setDateFinReelle($dateFinReelle)
    {
        $this->dateFinReelle = $dateFinReelle;

        return $this;
    }

    /**
     * Get dateFinReelle
     *
     * @return \DateTime
     */
    public function getDateFinReelle()
    {
        return $this->dateFinReelle;
    }

    /**
     * Set totalTempsPasse
     *
     * @param integer $totalTempsPasse
     *
     * @return allTicketsExports
     */
    public function setTotalTempsPasse($totalTempsPasse)
    {
        $this->totalTempsPasse = $totalTempsPasse;

        return $this;
    }

    /**
     * Get totalTempsPasse
     *
     * @return int
     */
    public function getTotalTempsPasse()
    {
        return $this->totalTempsPasse;
    }

    /**
     * Set totalTempsPasseMois
     *
     * @param integer $totalTempsPasseMois
     *
     * @return allTicketsExports
     */
    public function setTotalTempsPasseMois($totalTempsPasseMois)
    {
        $this->totalTempsPasseMois = $totalTempsPasseMois;

        return $this;
    }

    /**
     * Get totalTempsPasseMois
     *
     * @return int
     */
    public function getTotalTempsPasseMois()
    {
        return $this->totalTempsPasseMois;
    }

    /**
     * Set depacement
     *
     * @param integer $depacement
     *
     * @return allTicketsExports
     */
    public function setDepacement($depacement)
    {
        $this->depacement = $depacement;

        return $this;
    }

    /**
     * Get depacement
     *
     * @return int
     */
    public function getDepacement()
    {
        return $this->depacement;
    }

    /**
     * Set respectDateDebut
     *
     * @param integer $respectDateDebut
     *
     * @return allTicketsExports
     */
    public function setRespectDateDebut($respectDateDebut)
    {
        $this->respectDateDebut = $respectDateDebut;

        return $this;
    }

    /**
     * Get respectDateDebut
     *
     * @return int
     */
    public function getRespectDateDebut()
    {
        return $this->respectDateDebut;
    }

    /**
     * Set respectDateFin
     *
     * @param integer $respectDateFin
     *
     * @return allTicketsExports
     */
    public function setRespectDateFin($respectDateFin)
    {
        $this->respectDateFin = $respectDateFin;

        return $this;
    }

    /**
     * Get respectDateFin
     *
     * @return int
     */
    public function getRespectDateFin()
    {
        return $this->respectDateFin;
    }
}

