<?php

namespace Redmine\TestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ExportTicketByProject
 *
 * @ORM\Table(name="export_ticket_by_project")
 * @ORM\Entity(repositoryClass="Redmine\TestBundle\Repository\ExportTicketByProjectRepository")
 */
class ExportTicketByProject
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Nom_Projet", type="string", length=255)
     */
    private $nomProjet;

    /**
     * @var string
     *
     * @ORM\Column(name="Ticket", type="string", length=255)
     */
    private $ticket;

    /**
     * @var string
     *
     * @ORM\Column(name="user", type="string", length=255,nullable = true)
     */
    private $user;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_debut_plannifiee", type="datetime" , nullable = true)
     */
    private $dateDebutPlannifiee;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_fin_plannifiee", type="datetime" , nullable = true)
     */
    private $dateFinPlannifiee;

    /**
     * @var int
     *
     * @ORM\Column(name="temps_estime", type="integer" , nullable = true)
     */
    private $tempsEstime;

    /**
     * @var int
     *
     * @ORM\Column(name="avancement", type="integer" , nullable = true)
     */
    private $avancement;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_debut_reelle", type="datetime", nullable=true)
     */
    private $dateDebutReelle;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_fin_reelle", type="datetime", nullable=true)
     */
    private $dateFinReelle;

    /**
     * @var int
     *
     * @ORM\Column(name="total_temp_passee", type="integer" , nullable = true)
     */
    private $totalTempPassee;

    /**
     * @var int
     *
     * @ORM\Column(name="totale_temps_passe_mois", type="integer" , nullable = true)
     */
    private $totaleTempsPasseMois;

    /**
     * @var int
     *
     * @ORM\Column(name="depacement", type="integer" , nullable = true)
     */
    private $depacement;

    /**
     * @var int
     *
     * @ORM\Column(name="respect_date_debut", type="integer", nullable=true)
     */
    private $respectDateDebut;

    /**
     * @var int
     *
     * @ORM\Column(name="respect_date_fin", type="integer", nullable=true)
     */
    private $respectDateFin;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nomProjet
     *
     * @param string $nomProjet
     *
     * @return ExportTicketByProject
     */
    public function setNomProjet($nomProjet)
    {
        $this->nomProjet = $nomProjet;

        return $this;
    }

    /**
     * Get nomProjet
     *
     * @return string
     */
    public function getNomProjet()
    {
        return $this->nomProjet;
    }

    /**
     * Set ticket
     *
     * @param string $ticket
     *
     * @return ExportTicketByProject
     */
    public function setTicket($ticket)
    {
        $this->ticket = $ticket;

        return $this;
    }

    /**
     * Get ticket
     *
     * @return string
     */
    public function getTicket()
    {
        return $this->ticket;
    }

    /**
     * Set user
     *
     * @param string $user
     *
     * @return ExportTicketByProject
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return string
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set dateDebutPlannifiee
     *
     * @param \DateTime $dateDebutPlannifiee
     *
     * @return ExportTicketByProject
     */
    public function setDateDebutPlannifiee($dateDebutPlannifiee)
    {
        $this->dateDebutPlannifiee = $dateDebutPlannifiee;

        return $this;
    }

    /**
     * Get dateDebutPlannifiee
     *
     * @return \DateTime
     */
    public function getDateDebutPlannifiee()
    {
        return $this->dateDebutPlannifiee;
    }

    /**
     * Set dateFinPlannifiee
     *
     * @param \DateTime $dateFinPlannifiee
     *
     * @return ExportTicketByProject
     */
    public function setDateFinPlannifiee($dateFinPlannifiee)
    {
        $this->dateFinPlannifiee = $dateFinPlannifiee;

        return $this;
    }

    /**
     * Get dateFinPlannifiee
     *
     * @return \DateTime
     */
    public function getDateFinPlannifiee()
    {
        return $this->dateFinPlannifiee;
    }

    /**
     * Set tempsEstime
     *
     * @param integer $tempsEstime
     *
     * @return ExportTicketByProject
     */
    public function setTempsEstime($tempsEstime)
    {
        $this->tempsEstime = $tempsEstime;

        return $this;
    }

    /**
     * Get tempsEstime
     *
     * @return int
     */
    public function getTempsEstime()
    {
        return $this->tempsEstime;
    }

    /**
     * Set avancement
     *
     * @param integer $avancement
     *
     * @return ExportTicketByProject
     */
    public function setAvancement($avancement)
    {
        $this->avancement = $avancement;

        return $this;
    }

    /**
     * Get avancement
     *
     * @return int
     */
    public function getAvancement()
    {
        return $this->avancement;
    }

    /**
     * Set dateDebutReelle
     *
     * @param \DateTime $dateDebutReelle
     *
     * @return ExportTicketByProject
     */
    public function setDateDebutReelle($dateDebutReelle)
    {
        $this->dateDebutReelle = $dateDebutReelle;

        return $this;
    }

    /**
     * Get dateDebutReelle
     *
     * @return \DateTime
     */
    public function getDateDebutReelle()
    {
        return $this->dateDebutReelle;
    }

    /**
     * Set dateFinReelle
     *
     * @param \DateTime $dateFinReelle
     *
     * @return ExportTicketByProject
     */
    public function setDateFinReelle($dateFinReelle)
    {
        $this->dateFinReelle = $dateFinReelle;

        return $this;
    }

    /**
     * Get dateFinReelle
     *
     * @return \DateTime
     */
    public function getDateFinReelle()
    {
        return $this->dateFinReelle;
    }

    /**
     * Set totalTempPassee
     *
     * @param integer $totalTempPassee
     *
     * @return ExportTicketByProject
     */
    public function setTotalTempPassee($totalTempPassee)
    {
        $this->totalTempPassee = $totalTempPassee;

        return $this;
    }

    /**
     * Get totalTempPassee
     *
     * @return int
     */
    public function getTotalTempPassee()
    {
        return $this->totalTempPassee;
    }

    /**
     * Set totaleTempsPasseMois
     *
     * @param integer $totaleTempsPasseMois
     *
     * @return ExportTicketByProject
     */
    public function setTotaleTempsPasseMois($totaleTempsPasseMois)
    {
        $this->totaleTempsPasseMois = $totaleTempsPasseMois;

        return $this;
    }

    /**
     * Get totaleTempsPasseMois
     *
     * @return int
     */
    public function getTotaleTempsPasseMois()
    {
        return $this->totaleTempsPasseMois;
    }

    /**
     * Set depacement
     *
     * @param integer $depacement
     *
     * @return ExportTicketByProject
     */
    public function setDepacement($depacement)
    {
        $this->depacement = $depacement;

        return $this;
    }

    /**
     * Get depacement
     *
     * @return int
     */
    public function getDepacement()
    {
        return $this->depacement;
    }

    /**
     * Set respectDateDebut
     *
     * @param integer $respectDateDebut
     *
     * @return ExportTicketByProject
     */
    public function setRespectDateDebut($respectDateDebut)
    {
        $this->respectDateDebut = $respectDateDebut;

        return $this;
    }

    /**
     * Get respectDateDebut
     *
     * @return int
     */
    public function getRespectDateDebut()
    {
        return $this->respectDateDebut;
    }

    /**
     * Set respectDateFin
     *
     * @param integer $respectDateFin
     *
     * @return ExportTicketByProject
     */
    public function setRespectDateFin($respectDateFin)
    {
        $this->respectDateFin = $respectDateFin;

        return $this;
    }

    /**
     * Get respectDateFin
     *
     * @return int
     */
    public function getRespectDateFin()
    {
        return $this->respectDateFin;
    }
}

