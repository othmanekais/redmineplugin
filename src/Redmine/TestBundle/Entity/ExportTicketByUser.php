<?php

namespace Redmine\TestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ExportTicketByUser
 *
 * @ORM\Table(name="export_ticket_by_user")
 * @ORM\Entity(repositoryClass="Redmine\TestBundle\Repository\ExportTicketByUserRepository")
 */
class ExportTicketByUser
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Nom_Projet", type="string", length=255)
     */
    private $nomProjet;

    /**
     * @var string
     *
     * @ORM\Column(name="Ticket", type="string", length=255)
     */
    private $ticket;

    /**
     * @var string
     *
     * @ORM\Column(name="user", type="string", length=255)
     */
    private $user;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_debut_plannifiee", type="datetime" , nullable = true)
     */
    private $dateDebutPlannifiee;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_fin_plannifiee", type="datetime" , nullable = true)
     */
    private $dateFinPlannifiee;

    /**
     * @var int
     *
     * @ORM\Column(name="temps_estimee", type="integer" , nullable = true)
     */
    private $tempsEstimee;

    /**
     * @var int
     *
     * @ORM\Column(name="avancement", type="integer" , nullable = true)
     */
    private $avancement;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_debut_reelle", type="datetime", nullable=true , nullable = true)
     */
    private $dateDebutReelle;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_fin_reelle", type="datetime", nullable=true)
     */
    private $dateFinReelle;

    /**
     * @var int
     *
     * @ORM\Column(name="total_temps_passe", type="integer" , nullable = true)
     */
    private $totalTempsPasse;

    /**
     * @var int
     *
     * @ORM\Column(name="total_temps_passe_mois", type="integer" , nullable = true)
     */
    private $totalTempsPasseMois;

    /**
     * @var int
     *
     * @ORM\Column(name="depacement", type="integer" , nullable = true)
     */
    private $depacement;

    /**
     * @var int
     *
     * @ORM\Column(name="respect_date_debut", type="integer", nullable=true)
     */
    private $respectDateDebut;

    /**
     * @var int
     *
     * @ORM\Column(name="respect_date_fin", type="integer", nullable=true)
     */
    private $respectDateFin;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nomProjet
     *
     * @param string $nomProjet
     *
     * @return ExportTicketByUser
     */
    public function setNomProjet($nomProjet)
    {
        $this->nomProjet = $nomProjet;

        return $this;
    }

    /**
     * Get nomProjet
     *
     * @return string
     */
    public function getNomProjet()
    {
        return $this->nomProjet;
    }

    /**
     * Set ticket
     *
     * @param string $ticket
     *
     * @return ExportTicketByUser
     */
    public function setTicket($ticket)
    {
        $this->ticket = $ticket;

        return $this;
    }

    /**
     * Get ticket
     *
     * @return string
     */
    public function getTicket()
    {
        return $this->ticket;
    }

    /**
     * Set dateDebutPlannifiee
     *
     * @param \DateTime $dateDebutPlannifiee
     *
     * @return ExportTicketByUser
     */
    public function setDateDebutPlannifiee($dateDebutPlannifiee)
    {
        $this->dateDebutPlannifiee = $dateDebutPlannifiee;

        return $this;
    }

    /**
     * Get dateDebutPlannifiee
     *
     * @return \DateTime
     */
    public function getDateDebutPlannifiee()
    {
        return $this->dateDebutPlannifiee;
    }

    /**
     * Set dateFinPlannifiee
     *
     * @param \DateTime $dateFinPlannifiee
     *
     * @return ExportTicketByUser
     */
    public function setDateFinPlannifiee($dateFinPlannifiee)
    {
        $this->dateFinPlannifiee = $dateFinPlannifiee;

        return $this;
    }

    /**
     * Get dateFinPlannifiee
     *
     * @return \DateTime
     */
    public function getDateFinPlannifiee()
    {
        return $this->dateFinPlannifiee;
    }

    /**
     * Set tempsEstimee
     *
     * @param integer $tempsEstimee
     *
     * @return ExportTicketByUser
     */
    public function setTempsEstimee($tempsEstimee)
    {
        $this->tempsEstimee = $tempsEstimee;

        return $this;
    }

    /**
     * Get tempsEstimee
     *
     * @return int
     */
    public function getTempsEstimee()
    {
        return $this->tempsEstimee;
    }

    /**
     * Set avancement
     *
     * @param integer $avancement
     *
     * @return ExportTicketByUser
     */
    public function setAvancement($avancement)
    {
        $this->avancement = $avancement;

        return $this;
    }

    /**
     * Get avancement
     *
     * @return int
     */
    public function getAvancement()
    {
        return $this->avancement;
    }

    /**
     * Set dateDebutReelle
     *
     * @param \DateTime $dateDebutReelle
     *
     * @return ExportTicketByUser
     */
    public function setDateDebutReelle($dateDebutReelle)
    {
        $this->dateDebutReelle = $dateDebutReelle;

        return $this;
    }

    /**
     * Get dateDebutReelle
     *
     * @return \DateTime
     */
    public function getDateDebutReelle()
    {
        return $this->dateDebutReelle;
    }

    /**
     * Set dateFinReelle
     *
     * @param \DateTime $dateFinReelle
     *
     * @return ExportTicketByUser
     */
    public function setDateFinReelle($dateFinReelle)
    {
        $this->dateFinReelle = $dateFinReelle;

        return $this;
    }

    /**
     * Get dateFinReelle
     *
     * @return \DateTime
     */
    public function getDateFinReelle()
    {
        return $this->dateFinReelle;
    }

    /**
     * Set totalTempsPasse
     *
     * @param integer $totalTempsPasse
     *
     * @return ExportTicketByUser
     */
    public function setTotalTempsPasse($totalTempsPasse)
    {
        $this->totalTempsPasse = $totalTempsPasse;

        return $this;
    }

    /**
     * Get totalTempsPasse
     *
     * @return int
     */
    public function getTotalTempsPasse()
    {
        return $this->totalTempsPasse;
    }

    /**
     * Set totalTempsPasseMois
     *
     * @param integer $totalTempsPasseMois
     *
     * @return ExportTicketByUser
     */
    public function setTotalTempsPasseMois($totalTempsPasseMois)
    {
        $this->totalTempsPasseMois = $totalTempsPasseMois;

        return $this;
    }

    /**
     * Get totalTempsPasseMois
     *
     * @return int
     */
    public function getTotalTempsPasseMois()
    {
        return $this->totalTempsPasseMois;
    }

    /**
     * Set depacement
     *
     * @param integer $depacement
     *
     * @return ExportTicketByUser
     */
    public function setDepacement($depacement)
    {
        $this->depacement = $depacement;

        return $this;
    }

    /**
     * Get depacement
     *
     * @return int
     */
    public function getDepacement()
    {
        return $this->depacement;
    }

    /**
     * Set respectDateDebut
     *
     * @param integer $respectDateDebut
     *
     * @return ExportTicketByUser
     */
    public function setRespectDateDebut($respectDateDebut)
    {
        $this->respectDateDebut = $respectDateDebut;

        return $this;
    }

    /**
     * Get respectDateDebut
     *
     * @return int
     */
    public function getRespectDateDebut()
    {
        return $this->respectDateDebut;
    }

    /**
     * Set respectDateFin
     *
     * @param integer $respectDateFin
     *
     * @return ExportTicketByUser
     */
    public function setRespectDateFin($respectDateFin)
    {
        $this->respectDateFin = $respectDateFin;

        return $this;
    }

    /**
     * Get respectDateFin
     *
     * @return int
     */
    public function getRespectDateFin()
    {
        return $this->respectDateFin;
    }

    /**
     * Set user
     *
     * @param string $user
     *
     * @return ExportTicketByUser
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return string
     */
    public function getUser()
    {
        return $this->user;
    }
}

