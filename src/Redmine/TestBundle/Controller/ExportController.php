<?php

namespace Redmine\TestBundle\Controller;

use Redmine\TestBundle\Entity\allTicketsExports;
use Redmine\TestBundle\Entity\ExportTicketByProject;
use Redmine\TestBundle\Entity\ExportTicketByUser;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Redmine\TestBundle\Model\CsvResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use GuzzleHttp\Client as guzzle;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Translation\Loader\ArrayLoader;
use Symfony\Component\Translation\Translator;


class ExportController extends Controller
{

    /**
     * @return Response
     * @Route("/index",name="indexpage")
     */

    public function ReportsAllTicketAction(Request $request){

        //$buzz = $this->container->get('buzz');
        //$issues = $buzz->get('http://othmanekais:othmanekais@redmine.itroad.ma/issues.json');
        $client = new guzzle();
        $res = $client->request('GET', 'http://redmine.itroad.ma/issues.json?offset=0&limit=100', [
            'auth' => [$request->get('username'),$request->get('password') ]
        ]);
        //echo $res->getBody();die();

        //$session = new Session();
        //$session->start();
        $session =  $this->get('session');
        // set and get session attributes

        $session->set('name', $request->get('username'));
        $session->set('pass',$request->get('password'));
        $session->save();


        $issues_content = $res->getBody();
        $result= json_decode($issues_content);

        /*echo '<pre>';
            print_r($result);
            echo '</pre>'; die();*/
        $tickets = array();
        $projet = array();
        $i = 0;
            foreach($result->issues as $issue){
                if(isset($issue->assigned_to->name)) {
                    if (!in_array($issue->assigned_to->name, $tickets)) {
                        $tickets[] = $issue->assigned_to->name;
                        $i++;
                    }
                }

                if(isset($issue->project->name)) {
                    if (!in_array($issue->project->name, $projet)) {
                        $projet[] = $issue->project->name;
                        $i++;
                    }
                }
            }


            /*echo '<pre>';
            print_r($tickets);
            echo '</pre>'; die();*/
        return $this->render('RedmineTestBundle:Default:index.html.twig',array(
            'tickets' => $tickets,
            'projet' => $projet
        ));
    }

    /**
     * @Route("/projects",name="exports_all_tickets")
     * @param $request
     * @return Response
     */

    public function exportsAllProjTicketsAction( Request $request)
    {

        //$buzz = $this->container->get('buzz');
        $date_debut = $request->get('date_debut');
        $date_fin = $request->get('date_fin');
        $date_debut_reel = $request->get('date_debut_réelle');
        $date_fin_reel = $request->get('date_fin_réelle');
        $session = $this->get('session');
        $username = $session->get('name');
        $password = $session->get('pass');
        $request->setLocale("FR");
        //setlocale(LC_TIME, "fr_FR");
        //var_dump ($request->getLocale());die();

        $currentMonth = date('F');
        $last = Date('F', strtotime($currentMonth . " last month"));
        $translator = new Translator('fr_FR');
        $translator->addLoader('array', new ArrayLoader());
        $translator->addResource('array', array(
            'January' => 'Janvier',
            'February'=> 'Février',
            'March' => 'Mars',
            'April' => 'Avril',
            'May'   => 'Mai',
            'June' => 'Juin',
            'July' => 'Juillet',
            'August' => 'Août',
            'September' => 'Septembre',
            'October' => 'Octobre',
            'November' => 'Novembre',
            'December' => 'Décembre'

        ), 'fr_FR');

        $last_mounth = $translator->trans($last);


        $url = '';


        if($date_debut !== "" && $date_fin == ""){
            $url .=  'start_date=%3E%3D' . $date_debut;
        } elseif ($date_debut !== "" && $date_fin !== ""){
            $url .=  'start_date=%3E%3D' . $date_debut;
            $url .=  '&due_date=%3C%3D' . $date_fin;
        } elseif ($date_fin !== "" && $date_debut == ""){
            $url .=  'due_date=%3C%3D' . $date_fin;
        }


        if ($date_debut_reel !== "") {
            if($url !== "") {
                $url .=  '&cf_10=%3E%3D' . $date_debut_reel;
            } else {
                $url .=  'cf_10=' . $date_debut_reel;
            }
        }
        if ($date_fin_reel !== "") {
            if($url !== "") {
                $url .=  '&cf_11=%3C%3D' . $date_fin_reel;
            } else {
                $url .=  'cf_11=' . $date_fin_reel;
            }

        }

        $client = new guzzle();
        $res = $client->request('GET', 'http://redmine.itroad.ma/issues.json?offset=0&limit=100&'.$url, [
            'auth' => [$username, $password]
        ]);

        $issues_content = $res->getBody();

        //$issues = $buzz->get('http://othmanekais:othmanekais@redmine.itroad.ma/issues.json?'.$url);
        //$issues_content = $issues->getContent();
        $result= json_decode($issues_content);
        $tickets = array(array());
        $i = 0;
        $times = 0;


            /*echo '<pre>';
            print_r($result);
            echo '</pre>';*/

            foreach($result->issues as $key => $issue){
                $issues_spent_time = $client->request('GET', 'http://redmine.itroad.ma/issues/'.$issue->id.'.json', [
                    'auth' => [$username, $password]
                ]);
                //$issues_spent_time = $buzz->get('http://othmanekais:othmanekais@redmine.itroad.ma/issues/'.$issue->id.'.json');
                $issue_spent_time_in_mount = $client->request('GET', 'http://redmine.itroad.ma/time_entries.json?utf8=✓&period_type=1&period=last_month&issue_id='.$issue->id, [
                    'auth' => [$username, $password]
                ]);
                //$issue_spent_time_in_mount = $buzz->get('http://othmanekais:othmanekais@redmine.itroad.ma/issues/'.$issue->id.'.json?utf8=%E2%9C%93&set_filter=1&sort=spent_on%3Adesc&f%5B%5D=spent_on&op%5Bspent_on%5D=m&f%5B%5D=&c%5B%5D=spent_on&c%5B%5D=user&c%5B%5D=activity&c%5B%5D=issue&c%5B%5D=comments&c%5B%5D=hours&group_by=&t%5B%5D=hours&t%5B%5D=');
                $issues_spent_content_mounth = $issue_spent_time_in_mount->getBody();
                $issues_spent_content = $issues_spent_time->getBody();
                $result_time = json_decode($issues_spent_content);
                $result_time_mounth = json_decode($issues_spent_content_mounth);




                $project_name = $issue->project->name;
                //$tracker_name = $issue->tracker->name;
                //$status_name  = $issue->status->name;
                //$priority_name = $issue->priority->name;
                $subject = $issue->subject;
                //$assigned_to = $issue->author->name;
                if(isset($issue->start_date)){
                    $start_date = $issue->start_date;
                }

                //$updated_on = $issue->updated_on;


                            $tickets[$i]['Project Name'] = $project_name;
                            //$tickets[$i]['Tracker Name'] = $tracker_name;
                            //$tickets[$i]['Status Name'] = $status_name;
                            //$tickets[$i]['Priority Name'] = $priority_name;
                            $tickets[$i]['Subject'] = $subject;
                            //$tickets[$i]['Assingned To'] = $assigned_to;
                            $tickets[$i]['Start Date'] = $start_date;
                            //$tickets[$i]['Updated on'] = $updated_on;
                            if(isset($issue->estimated_hours)){
                                $tickets[$i]['Estimaed Hours'] = $issue->estimated_hours;

                            }
                            if(isset($issue->due_date)){
                                $tickets[$i]['Due Date'] = $issue->due_date;

                            }
                            if(isset($issue->done_ratio)){
                                $tickets[$i]['isDone %'] = $issue->done_ratio.'%';

                            }
                            //$tickets[$i]['Spent Time'] = $result_time->issue->spent_hours;
                            if(isset($result_time->issue->spent_hours)){
                                $tickets[$i]['total Spent Time'] = $result_time->issue->spent_hours;
                            }

                            foreach($result_time_mounth->time_entries as $time){
                                if(isset($time->hours)){
                                     $times += $time->hours;
                                }
                            }
                $tickets[$i]['total Spent Time Mount'] = $times;


                            foreach ($issue->custom_fields as $value){
                                if($value->id == 10) {
                                    if(isset($value->value) == ""){
                                        $tickets[$i]['date_début-réelle'] = 'Non Remplie';
                                    }
                                    else {
                                        $tickets[$i]['date_début-réelle'] = $value->value;
                                    }
                                }
                                 if($value->id == 11) {
                                     if(isset($value->value) == ""){
                                         $tickets[$i]['date_fin-réelle'] = 'Non Remplie';
                                     }
                                     else {
                                         $tickets[$i]['date_fin-réelle'] = $value->value;
                                     }


                                 }
                }
                            if(isset($tickets[$i]['Estimaed Hours']) > 0 AND isset($tickets[$i]['total Spent Time'])) {
                                $tickets[$i]['depassement'] = round((($tickets[$i]['total Spent Time'] - $tickets[$i]['Estimaed Hours']) / $tickets[$i]['Estimaed Hours']) * 100 , 0) . '%';

                            }

                            if($tickets[$i]['date_début-réelle'] !== 'Non Remplie') {
                                if ($tickets[$i]['date_début-réelle'] == $tickets[$i]['Start Date']) {
                                    $tickets[$i]['respcet_date_debut'] = 1;
                                } else {
                                    $tickets[$i]['respcet_date_debut'] = 0;
                                }
                            }

                            if($tickets[$i]['date_fin-réelle'] !== 'Non Remplie') {
                                if($tickets[$i]['date_fin-réelle'] == isset($tickets[$i]['Due Date'])){
                                    $tickets[$i]['respcet_date_fin'] = 1;
                                }else {
                                    $tickets[$i]['respcet_date_fin'] = 0;
                                }

                            }

                $i++;
                $times = 0;

              }


        /*echo '<pre>';
        print_r($tickets);
        echo '</pre>';*/


        //$this->array_to_csv_download($tickets);


        //$columns = array('project_name','tracker_name','status_name','priority_name','subject','assigned_To','updated_on');
            //$response = new CSVResponse('tickets.csv', $tickets, 200, $columns );
            //return $response;

        // Insert Data in Database


        for($j=0;$j<count($tickets);$j++){
            $expallticket = new allTicketsExports();
            $expallticket->setNomProjet($tickets[$j]['Project Name'])
                ->setTicket($tickets[$j]['Subject']);
                if(isset($tickets[$j]['Estimaed Hours'])){
                    $expallticket->setTempsEstime($tickets[$j]['Estimaed Hours']);
                }
            if(isset($tickets[$j]['Due Date'])){
                $expallticket->setDateFinPlannifiee(new \DateTime($tickets[$j]['Due Date']));
            }
                $expallticket->setDateDebutPlannifie(new \DateTime($tickets[$j]['Start Date']))
                ->setAvancement($tickets[$j]['isDone %']);
            if($tickets[$j]['date_début-réelle'] == 'Non Remplie') {
                $expallticket->setDateDebutReelle(null);
            } else {
                $expallticket->setDateDebutReelle(new \DateTime($tickets[$j]['date_début-réelle']));

            }


                if($tickets[$j]['date_fin-réelle'] == 'Non Remplie') {
                    $expallticket->setDateFinReelle(null);
                } else {
                    $expallticket->setDateFinReelle(new \DateTime($tickets[$j]['date_fin-réelle']));

                }
                if(isset($tickets[$j]['total Spent Time'])){
                    $expallticket->setTotalTempsPasse($tickets[$j]['total Spent Time']);
                }
                if(isset($tickets[$j]['total Spent Time Mount'])){
                    $expallticket->setTotalTempsPasseMois($tickets[$j]['total Spent Time Mount']);

                }

                if(isset($tickets[$j]['depassement'])){
                    $expallticket->setDepacement($tickets[$j]['depassement']);
                }


            if(isset($tickets[$j]['respcet_date_debut'])){
                $expallticket->setRespectDateDebut($tickets[$j]['respcet_date_debut']);
            } else {
                $expallticket->setRespectDateDebut(null);
            }

                if(isset($tickets[$j]['respcet_date_fin'])){
                    $expallticket->setRespectDateFin($tickets[$j]['respcet_date_fin']);
                } else {
                    $expallticket->setRespectDateFin(null);
                }


            $em = $this->getDoctrine()->getManager();
            $em->persist($expallticket);
            $em->flush();
        }






        $k = 3;
        $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();
        $styleArray = array(
            'borders' => array(
                'allborders' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );
        $phpExcelObject->getDefaultStyle()->applyFromArray($styleArray);
        $phpExcelObject->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Ressource')
            ->setCellValue('A2','Nom Projet')
            //->setCellValue('B2', 'Type Ticket')
            //->setCellValue('C2', 'Status')
            //->setCellValue('D2',' Priotité')
            ->setCellValue('B2','Ticket')
            //->setCellValue('F2','Attribué A')
            ->setCellValue('C2','Date début plannifiée')
            //->setCellValue('H2','Date Mise à jour')
            ->setCellValue('D2','Temps Estimé')
            ->setCellValue('E2','Date fin plannifiée')
            ->setCellValue('F2','Avancement')
            ->setCellValue('G2','Date début Réelle')
            ->setCellValue('H2','Date fin Réelle')
            //->setCellValue('N2','Temps Passé')
            ->setCellValue('I2','Totale Temps Passé')
            ->setCellValue('J2','Totale Temps Passé ( '.$last_mounth.' )')
            ->setCellValue('K2','Dépacement')
            ->setCellValue('L2','Respect Date Debut')
            ->setCellValue('M2','Respect Date Fin');

        $phpExcelObject->getActiveSheet()->mergeCells('A1:B1');
        $phpExcelObject->getActiveSheet()->mergeCells('B1:C1');
        $phpExcelObject->getActiveSheet()->mergeCells('C1:D1');
        $phpExcelObject->getActiveSheet()->mergeCells('D1:E1');
        $phpExcelObject->getActiveSheet()->mergeCells('E1:F1');
        $phpExcelObject->getActiveSheet()->mergeCells('F1:G1');
        $phpExcelObject->getActiveSheet()->mergeCells('G1:H1');
        $phpExcelObject->getActiveSheet()->mergeCells('H1:I1');
        $phpExcelObject->getActiveSheet()->mergeCells('I1:J1');
        $phpExcelObject->getActiveSheet()->mergeCells('J1:K1');
        $phpExcelObject->getActiveSheet()->mergeCells('J1:L1');
        $phpExcelObject->getActiveSheet()->mergeCells('L1:M1');
        //$phpExcelObject->getActiveSheet()->mergeCells('M1:N1');
        //$phpExcelObject->getActiveSheet()->mergeCells('N1:O1');
        //$phpExcelObject->getActiveSheet()->mergeCells('O1:P1');
        //$phpExcelObject->getActiveSheet()->mergeCells('P1:Q1');
        //$phpExcelObject->getActiveSheet()->mergeCells('Q1:R1');
        //$phpExcelObject->getActiveSheet()->mergeCells('R1:S1');


        /*----------------------------------------------------------------------------------------------*/


        for($i=0;$i<count($tickets);$i++){
            $phpExcelObject->setActiveSheetIndex(0)
                ->setCellValue('A'.$k, $tickets[$i]['Project Name'])
                //->setCellValue('B'.$k, $tickets[$i]['Tracker Name'])
                //->setCellValue('C'.$k, $tickets[$i]['Status Name'])
                //->setCellValue('D'.$k,$tickets[$i]['Priority Name'])
                ->setCellValue('B'.$k,$tickets[$i]['Subject'])
                //->setCellValue('F'.$k,$tickets[$i]['Assingned To'])
                ->setCellValue('C'.$k,$tickets[$i]['Start Date']);
                //->setCellValue('D'.$k,$tickets[$i]['Updated on'])
                    if(isset($tickets[$i]['Estimaed Hours'])){
                        $phpExcelObject->setActiveSheetIndex(0)->setCellValue('D'.$k,$tickets[$i]['Estimaed Hours']);
                    }
                if(isset($tickets[$i]['Due Date'])){
                    $phpExcelObject->setActiveSheetIndex(0)->setCellValue('E'.$k,$tickets[$i]['Due Date']);
                }
                $phpExcelObject->setActiveSheetIndex(0)
                ->setCellValue('F'.$k,$tickets[$i]['isDone %'])
                ->setCellValue('G'.$k,$tickets[$i]['date_début-réelle'])
                ->setCellValue('H'.$k,$tickets[$i]['date_fin-réelle']);
                //->setCellValue('N'.$k,$tickets[$i]['Spent Time'])
                    if(isset($tickets[$i]['total Spent Time'])){
                $phpExcelObject->setActiveSheetIndex(0)->setCellValue('I'.$k,$tickets[$i]['total Spent Time']);
                    }
                if(isset($tickets[$i]['total Spent Time Mount'])){
                        $phpExcelObject->setActiveSheetIndex(0)->setCellValue('J'.$k,$tickets[$i]['total Spent Time Mount']);
                }
                if(isset($tickets[$i]['depassement'])){
                    $phpExcelObject->setActiveSheetIndex(0)->setCellValue('K'.$k,$tickets[$i]['depassement']);
                }

            $styleQ = $phpExcelObject->getActiveSheet()->getStyle('K'.$k);
            $styleQ->applyFromArray(
                array('fill' => array('type' => \PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => 'FA8072'))
                ));

            if(isset($tickets[$i]['respcet_date_debut'])){
                $phpExcelObject->setActiveSheetIndex(0)->setCellValue('L'.$k,$tickets[$i]['respcet_date_debut']);
                if($tickets[$i]['respcet_date_debut'] == 0) {
                    $styleR = $phpExcelObject->getActiveSheet()->getStyle('L'.$k);
                    $styleR->applyFromArray(
                        array('fill' => array('type' => \PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => '98FB98'))
                        ));
                } else {
                    $styleR = $phpExcelObject->getActiveSheet()->getStyle('L'.$k);
                    $styleR->applyFromArray(
                        array('fill' => array('type' => \PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => '008000'))
                        ));
                }
            }
            if(isset($tickets[$i]['respcet_date_fin'])){
                $phpExcelObject->setActiveSheetIndex(0)->setCellValue('M'.$k,$tickets[$i]['respcet_date_fin']);
                if($tickets[$i]['respcet_date_fin'] == 0) {
                    $styleR = $phpExcelObject->getActiveSheet()->getStyle('M'.$k);
                    $styleR->applyFromArray(
                        array('fill' => array('type' => \PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => '98FB98'))
                        ));
                } else {
                    $styleR = $phpExcelObject->getActiveSheet()->getStyle('M'.$k);
                    $styleR->applyFromArray(
                        array('fill' => array('type' => \PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => '008000'))
                        ));
                }
            }
            $k = $k + 1;
        }


        $phpExcelObject->getActiveSheet()->getColumnDimension('A')->setWidth(15);
        $phpExcelObject->getActiveSheet()->getColumnDimension('B')->setWidth(40);
        $phpExcelObject->getActiveSheet()->getColumnDimension('C')->setWidth(22);
        $phpExcelObject->getActiveSheet()->getColumnDimension('D')->setWidth(17);
        $phpExcelObject->getActiveSheet()->getColumnDimension('E')->setWidth(20);
        $phpExcelObject->getActiveSheet()->getColumnDimension('F')->setWidth(15);
        $phpExcelObject->getActiveSheet()->getColumnDimension('G')->setWidth(20);
        $phpExcelObject->getActiveSheet()->getColumnDimension('H')->setWidth(25);
        $phpExcelObject->getActiveSheet()->getColumnDimension('I')->setWidth(23);
        $phpExcelObject->getActiveSheet()->getColumnDimension('J')->setWidth(40);
        $phpExcelObject->getActiveSheet()->getColumnDimension('K')->setWidth(15);
        $phpExcelObject->getActiveSheet()->getColumnDimension('L')->setWidth(20);
        $phpExcelObject->getActiveSheet()->getColumnDimension('M')->setWidth(20);


        foreach(range('A','M') as $i) {
            $style = $phpExcelObject->getActiveSheet()->getStyle($i.'2');
            $styleFont = $style->getFont();
            $styleFont->setBold(true);
            $styleFont->setSize(10);
            $styleFont->setName('Arial');
            $style->applyFromArray(
                array('fill' => array('type' => \PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => '00BFFF'))
                ));
            }



        $styleA1 = $phpExcelObject->getActiveSheet()->getStyle('A1');
        $styleFont = $styleA1->getFont();
        $styleFont->setBold(true);
        $styleFont->setSize(12);
        $styleFont->setName('Arial');
        $styleFont->getColor()->setARGB(\PHPExcel_Style_Color::COLOR_WHITE);
        $styleA1->applyFromArray(
            array('fill' => array('type' => \PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => '000000'))
            ));

        foreach(range('C','M') as $j){
            for($i=3;$i<count($tickets);$i++){
                if($phpExcelObject->getActiveSheet()->getCell($j.$i)->getValue() == 0 OR $phpExcelObject->getActiveSheet()->getCell($j.$i)->getValue() == null ){
                    $phpExcelObject->getActiveSheet()->setCellValue($j.$i,null);
                    $style = $phpExcelObject->getActiveSheet()->getStyle($j.$i);
                    $style->applyFromArray(
                        array('fill' => array('type' => \PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => '808080'))
                        ));
                }
            }
        }


        $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel5');
        // create the response
        $response = $this->get('phpexcel')->createStreamedResponse($writer);
        // adding headers
        $dispositionHeader = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            'export-ticket.xls'
        );
        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');
        $response->headers->set('Content-Disposition', $dispositionHeader);

        return $response;

        return $this->render('RedmineTestBundle:Default:index.html.twig');
    }

    /**
     * @return Response
     * @Route("/project" , name="exportsby_project")
     */

    public function exportProjectNameAction(Request $request)
    {
        //Cette Methode retourne les tickets assignée à un projet donné en argument
        //$buzz = $this->container->get('buzz');
        //$project = $buzz->get('http://localhost:80/redmine/projects.json');
        $client = new guzzle();
        $session = $this->get('session');
        $username = $session->get('name');
        $password = $session->get('pass');
        $res = $client->request('GET', 'http://redmine.itroad.ma/projects.json', [
            'auth' => [$username, $password]
        ]);
        $projects = $res->getBody();
        $result = json_decode($projects);
        $tickets = array(array());
        $i = 0;
        $name = $request->get('project');
        $times = 0;
        $currentMonth = date('F');
        $last = Date('F', strtotime($currentMonth . " last month"));
        $translator = new Translator('fr_FR');
        $translator->addLoader('array', new ArrayLoader());
        $translator->addResource('array', array(
            'January' => 'Janvier',
            'February'=> 'Février',
            'March' => 'Mars',
            'April' => 'Avril',
            'May'   => 'Mai',
            'June' => 'Juin',
            'July' => 'Juillet',
            'August' => 'Août',
            'September' => 'Septembre',
            'October' => 'Octobre',
            'November' => 'Novembre',
            'December' => 'Décembre'

        ), 'fr_FR');

        $last_mounth = $translator->trans($last);



        /*echo '<pre>';
        print_r($result);
        echo '</pre>';*/

        foreach($result->projects as $value) {

            //var_dump($value->name); echo '<br>';
            //var_dump($name);die();

            if (str_replace(' ', '', $value->name) == str_replace(' ', '', $name)) {

                /*$issues = $buzz->get('http://localhost/redmine/projects/' . $name . '/issues.json');
                $issues_content = $issues->getContent();
                $result_issues = json_decode($issues_content);*/

                     $date_debut = $request->get('date_debut');
                     $date_fin = $request->get('date_fin');
                     $date_debut_reel = $request->get('date_debut_réelle');
                     $date_fin_reel = $request->get('date_fin_réelle');

                     $url = '';



                if($date_debut !== "" && $date_fin == ""){
                    $url .=  'start_date=' . $date_debut;
                } elseif ($date_debut !== "" && $date_fin !== ""){
                    $url .=  'start_date=%3E%3D' . $date_debut;
                    $url .=  '&due_date=%3C%3D' . $date_fin;
                } elseif ($date_fin !== "" && $date_debut == ""){
                    $url .=  'due_date=' . $date_fin;
                }

                     if ($date_debut_reel !== "") {
                        if($url !== "") {
                            $url .=  '&cf_10=%3E%3D' . $date_debut_reel;
                        } else {
                            $url .=  'cf_10=' . $date_debut_reel;
                        }

                     }
                     if ($date_fin_reel !== "") {
                        if($url !== "") {
                            $url .=  '&cf_11=%3C%3D' . $date_fin_reel;
                        } else {
                            $url .=  'cf_11=' . $date_fin_reel;
                        }

                     }

                $client = new guzzle();
                $field = $client->request('GET', 'http://redmine.itroad.ma/projects/'.$value->identifier . '/issues.json?offset=0&limit=100&' .$url, [
                    'auth' => [$username, $password]
                ]);
                    //$field = $buzz->get('http://localhost/redmine/projects/' . $name . '/issues.json?' .$url );
                    //$field = $buzz->get('http://localhost/redmine/projects/' . $name . '/issues.json?start_date=%3E%3D' . $date_debut . '&due_date=%3C%3D' . $date_fin);
                    $field_content = $field->getBody();
                    $result_field = json_decode($field_content);
                    foreach ($result_field->issues as $issues_list) {
                        $issues_spent_time = $client->request('GET', 'http://redmine.itroad.ma/issues/'. $issues_list->id . '.json', [
                            'auth' => [$username, $password]
                        ]);

                        //$issues_spent_time = $buzz->get('http://localhost/redmine/issues/' . $issues_list->id . '.json');
                        $issues_spent_time_content = $issues_spent_time->getBody();
                        $result_issues_spent = json_decode($issues_spent_time_content);
                        //$issue_spent_time_in_mount = $buzz->get('http://localhost/redmine/issues/'.$issues_list->id.'.json?utf8=%E2%9C%93&set_filter=1&sort=spent_on%3Adesc&f%5B%5D=spent_on&op%5Bspent_on%5D=m&f%5B%5D=&c%5B%5D=spent_on&c%5B%5D=user&c%5B%5D=activity&c%5B%5D=issue&c%5B%5D=comments&c%5B%5D=hours&group_by=&t%5B%5D=hours&t%5B%5D=');
                        $issue_spent_time_in_mount = $client->request('GET', 'http://redmine.itroad.ma/time_entries.json?utf8=✓&period_type=1&period=last_month&issue_id='.$issues_list->id, [
                            'auth' => [$username, $password]
                        ]);
                        $issues_spent_content_mounth = $issue_spent_time_in_mount->getBody();
                        $result_time_mounth = json_decode($issues_spent_content_mounth);
                        //-------------------------------------------------------------/-------------------/
                        $tickets[$i]['Project'] = $value->name;
                        //$tickets[$i]['Tracker'] = $issues_list->tracker->name;
                        //$tickets[$i]['Status'] = $issues_list->status->name;
                        //$tickets[$i]['Priority'] = $issues_list->priority->name;
                        //$tickets[$i]['author'] = $issues_list->author->name;
                        if(isset($issues_list->assigned_to->name)){
                            $tickets[$i]['User'] = $issues_list->assigned_to->name;
                        }

                        $tickets[$i]['Subject'] = $issues_list->subject;
                        $tickets[$i]['start_date'] = $issues_list->start_date;
                        if(isset($issues_list->due_date)){
                            $tickets[$i]['Due Date'] = $issues_list->due_date;
                        }
                        if(isset($issues_list->done_ratio)){
                            $tickets[$i]['isDone'] = $issues_list->done_ratio;
                        }
                        //$tickets[$i]['Updated on'] = $issues_list->updated_on;
                        if(isset($issues_list->estimated_hours)){
                            $tickets[$i]['Estimated Hours'] = $issues_list->estimated_hours;
                        }

                        //$tickets[$i]['Spent Time'] = $result_issues_spent->issue->spent_hours;
                        $tickets[$i]['total Spent Time'] = $result_issues_spent->issue->spent_hours;
                        foreach($result_time_mounth->time_entries as $time){
                            if(isset($time->hours)){
                                $times += $time->hours;
                            }
                        }
                        $tickets[$i]['total Spent Time Mount'] = $times;



                        foreach ($issues_list->custom_fields as $custom) {
                            if ($custom->id == 10) {
                                if (isset($custom->value) == "") {
                                    $tickets[$i]['Date Debut réele'] = 'Non Remplie';
                                } else {
                                    $tickets[$i]['Date Debut réele'] = $custom->value;
                                }
                            }


                            if ($custom->id == 11) {

                                if (isset($custom->value) == "") {
                                    $tickets[$i]['Date Fin réele'] = 'Non Remplie';
                                } else {
                                    $tickets[$i]['Date Fin réele'] = $custom->value;
                                }

                            }
                        }

                        if(isset($tickets[$i]['Estimated Hours']) > 0) {
                            $tickets[$i]['depassement'] = round((($tickets[$i]['total Spent Time'] - $tickets[$i]['Estimated Hours']) / $tickets[$i]['Estimated Hours']) * 100 , 0) . '%';

                        }

                        if($tickets[$i]['Date Debut réele'] !== 'Non Remplie') {
                            if ($tickets[$i]['Date Debut réele'] == $tickets[$i]['start_date']) {
                                $tickets[$i]['respcet_date_debut'] = 1;
                            } else {
                                $tickets[$i]['respcet_date_debut'] = 0;
                            }
                        }

                        if($tickets[$i]['Date Fin réele'] !== 'Non Remplie') {
                            if (isset($tickets[$i]['Due Date'])) {
                                if ($tickets[$i]['Date Fin réele'] == $tickets[$i]['Due Date']) {
                                    $tickets[$i]['respcet_date_fin'] = 1;
                                } else {
                                    $tickets[$i]['respcet_date_fin'] = 0;
                                }

                            }
                        }
                        $times = 0;

                        $i++;

                    }

                /*echo '<pre>';
                print_r($result_custom_field_debut);
                echo '</pre>'; die();*/


                /*if($request->get('date_fin_réelle') !== null) {
                    $date_fin_reel = $request->get('date_fin_réelle');
                    $custom_field_fin = $buzz->get('http://localhost/redmine/projects/'.$name.'/issues.json?cf_1=%3C%3D'.$date_fin_reel);
                    $custom_field_fin_content = $custom_field_fin->getContent();
                    $result_custom_field_fin = json_decode($custom_field_fin_content);

                }*/

            }

        }


        for($j=0;$j<count($tickets);$j++){
            $expallticket = new ExportTicketByProject();
            $expallticket->setNomProjet($tickets[$j]['Project'])
                ->setTicket($tickets[$j]['Subject']);
            if(isset($tickets[$j]['Estimated Hours'])){
                $expallticket->setTempsEstime($tickets[$j]['Estimated Hours']);
            }
            if(isset($tickets[$j]['Due Date'])){
                $expallticket->setDateFinPlannifiee(new \DateTime($tickets[$j]['Due Date']));
            }
            $expallticket
                ->setDateDebutPlannifiee(new \DateTime($tickets[$j]['start_date']))
                ->setAvancement($tickets[$j]['isDone']);
            if(isset($tickets[$j]['User'])){
                $expallticket ->setUser($tickets[$j]['User']);
            }

            if($tickets[$j]['Date Debut réele'] == 'Non Remplie') {
                $expallticket->setDateDebutReelle(null);
            } else {
                $expallticket->setDateDebutReelle(new \DateTime($tickets[$j]['Date Debut réele']));

            }


            if($tickets[$j]['Date Fin réele'] == 'Non Remplie') {
                $expallticket->setDateFinReelle(null);
            } else {
                $expallticket->setDateFinReelle(new \DateTime($tickets[$j]['Date Fin réele']));

            }
            if(isset($tickets[$j]['total Spent Time'])){
                $expallticket->setTotalTempPassee($tickets[$j]['total Spent Time']);
            }
            if(isset($tickets[$j]['total Spent Time Mount'])){
                $expallticket->setTotaleTempsPasseMois($tickets[$j]['total Spent Time Mount']);
            }
            if(isset($tickets[$j]['depassement'])){
                $expallticket->setDepacement($tickets[$j]['depassement']);
            }

            if(isset($tickets[$j]['respcet_date_debut'])){
                $expallticket->setRespectDateDebut($tickets[$j]['respcet_date_debut']);
            } else {
                $expallticket->setRespectDateDebut(null);
            }

            if(isset($tickets[$j]['respcet_date_fin'])){
                $expallticket->setRespectDateFin($tickets[$j]['respcet_date_fin']);
            } else {
                $expallticket->setRespectDateFin(null);
            }


            $em = $this->getDoctrine()->getManager();
            $em->persist($expallticket);
            $em->flush();
        }

        $k = 3;
        $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();
        $styleArray = array(
            'borders' => array(
                'allborders' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );
        $phpExcelObject->getDefaultStyle()->applyFromArray($styleArray);
        $phpExcelObject->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Ressource')
            ->setCellValue('A2','Nom Projet')
            //->setCellValue('B2', 'Type Ticket')
            //->setCellValue('C2', 'Status')
            //->setCellValue('D2',' Priotité')
            ->setCellValue('B2','Ticket')
            ->setCellValue('C2','Attribué A')
            ->setCellValue('D2','Date début plannifiée')
            //->setCellValue('H2','Date Mise à jour')
            ->setCellValue('E2','Temps Estimé')
            ->setCellValue('F2','Date fin plannifiée')
            ->setCellValue('G2','Avancement')
            ->setCellValue('H2','Date début Réelle')
            ->setCellValue('I2','Date fin Réelle')
            //->setCellValue('N2','Temps Passé')
            ->setCellValue('J2','Totale Temps Passé')
            ->setCellValue('K2','Totale Temps Passé ('.$last_mounth.')')
            ->setCellValue('L2','Dépacement')
            ->setCellValue('M2','Respect Date Debut')
            ->setCellValue('N2','Respect Date Fin');

        $phpExcelObject->getActiveSheet()->mergeCells('A1:B1');
        $phpExcelObject->getActiveSheet()->mergeCells('B1:C1');
        $phpExcelObject->getActiveSheet()->mergeCells('C1:D1');
        $phpExcelObject->getActiveSheet()->mergeCells('D1:E1');
        $phpExcelObject->getActiveSheet()->mergeCells('E1:F1');
        $phpExcelObject->getActiveSheet()->mergeCells('F1:G1');
        $phpExcelObject->getActiveSheet()->mergeCells('G1:H1');
        $phpExcelObject->getActiveSheet()->mergeCells('H1:I1');
        $phpExcelObject->getActiveSheet()->mergeCells('I1:J1');
        $phpExcelObject->getActiveSheet()->mergeCells('J1:K1');
        $phpExcelObject->getActiveSheet()->mergeCells('J1:L1');
        $phpExcelObject->getActiveSheet()->mergeCells('L1:M1');
        $phpExcelObject->getActiveSheet()->mergeCells('M1:N1');

        /*----------------------------------------------------------------------------------------------*/


        for($i=0;$i<count($tickets);$i++){
            $phpExcelObject->setActiveSheetIndex(0)
                ->setCellValue('A'.$k, $tickets[$i]['Project'])
                //->setCellValue('B'.$k, $tickets[$i]['Tracker'])
                //->setCellValue('C'.$k, $tickets[$i]['Status'])
                //->setCellValue('D'.$k,$tickets[$i]['Priority'])
                ->setCellValue('B'.$k,$tickets[$i]['Subject']);
                if(isset($tickets[$i]['User'])){
                $phpExcelObject->setActiveSheetIndex(0)->setCellValue('C'.$k,$tickets[$i]['User']);
                }
                $phpExcelObject->setActiveSheetIndex(0)
                ->setCellValue('D'.$k,$tickets[$i]['start_date']);
                //->setCellValue('H'.$k,$tickets[$i]['Updated on']);
                    if(isset($tickets[$i]['Estimated Hours'])){
                    $phpExcelObject->setActiveSheetIndex(0)->setCellValue('E'.$k,$tickets[$i]['Estimated Hours']);
                    }
                if(isset($tickets[$i]['Due Date'])){
                    $phpExcelObject->setActiveSheetIndex(0)->setCellValue('F'.$k,$tickets[$i]['Due Date']);
                }
                $phpExcelObject->setActiveSheetIndex(0)
                ->setCellValue('G'.$k,$tickets[$i]['isDone'].'%');
                    if(isset($tickets[$i]['Date Debut réele'])){
                    $phpExcelObject->setActiveSheetIndex(0)->setCellValue('H'.$k,$tickets[$i]['Date Debut réele']);
                    }
                if(isset($tickets[$i]['Date Fin réele'])){
                        $phpExcelObject->setActiveSheetIndex(0)->setCellValue('I'.$k,$tickets[$i]['Date Fin réele']);
                }
                $phpExcelObject->setActiveSheetIndex(0)
                //->setCellValue('N'.$k,$tickets[$i]['Spent Time'])
                ->setCellValue('J'.$k,$tickets[$i]['total Spent Time'])
                ->setCellValue('K'.$k,$tickets[$i]['total Spent Time Mount']);
                if(isset($tickets[$i]['depassement'])){
                    $phpExcelObject->setActiveSheetIndex(0)->setCellValue('L'.$k,$tickets[$i]['depassement']);
                }

            $styleP = $phpExcelObject->getActiveSheet()->getStyle('L'.$k);
            $styleP->applyFromArray(
                array('fill' => array('type' => \PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => 'FA8072'))
                ));
            if(isset($tickets[$i]['respcet_date_debut'])){
                $phpExcelObject->setActiveSheetIndex(0)->setCellValue('M'.$k,$tickets[$i]['respcet_date_debut']);
                if($tickets[$i]['respcet_date_debut'] == 0) {
                    $styleR = $phpExcelObject->getActiveSheet()->getStyle('M'.$k);
                    $styleR->applyFromArray(
                        array('fill' => array('type' => \PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => '98FB98'))
                        ));
                } else {
                    $styleR = $phpExcelObject->getActiveSheet()->getStyle('M'.$k);
                    $styleR->applyFromArray(
                        array('fill' => array('type' => \PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => '008000'))
                        ));
                }
            }

            if(isset($tickets[$i]['respcet_date_fin'])){
                $phpExcelObject->setActiveSheetIndex(0)->setCellValue('N'.$k,$tickets[$i]['respcet_date_fin']);
                if($tickets[$i]['respcet_date_fin'] == 0) {
                    $styleR = $phpExcelObject->getActiveSheet()->getStyle('N'.$k);
                    $styleR->applyFromArray(
                        array('fill' => array('type' => \PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => '98FB98'))
                        ));
                } else {
                    $styleR = $phpExcelObject->getActiveSheet()->getStyle('N'.$k);
                    $styleR->applyFromArray(
                        array('fill' => array('type' => \PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => '008000'))
                        ));
                }
            }

            $k = $k + 1;
        }


        $phpExcelObject->getActiveSheet()->getColumnDimension('A')->setWidth(20);
        $phpExcelObject->getActiveSheet()->getColumnDimension('B')->setWidth(30);
        $phpExcelObject->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        $phpExcelObject->getActiveSheet()->getColumnDimension('D')->setWidth(23);
        $phpExcelObject->getActiveSheet()->getColumnDimension('E')->setWidth(15);
        $phpExcelObject->getActiveSheet()->getColumnDimension('F')->setWidth(20);
        $phpExcelObject->getActiveSheet()->getColumnDimension('G')->setWidth(10);
        $phpExcelObject->getActiveSheet()->getColumnDimension('H')->setWidth(20);
        $phpExcelObject->getActiveSheet()->getColumnDimension('I')->setWidth(20);
        $phpExcelObject->getActiveSheet()->getColumnDimension('J')->setWidth(25);
        $phpExcelObject->getActiveSheet()->getColumnDimension('K')->setWidth(25);
        $phpExcelObject->getActiveSheet()->getColumnDimension('L')->setWidth(20);
        $phpExcelObject->getActiveSheet()->getColumnDimension('M')->setWidth(25);
        $phpExcelObject->getActiveSheet()->getColumnDimension('N')->setWidth(20);


        foreach(range('A','N') as $i) {
            $style = $phpExcelObject->getActiveSheet()->getStyle($i.'2');
            $styleFont = $style->getFont();
            $styleFont->setBold(true);
            $styleFont->setSize(10);
            $styleFont->setName('Arial');
        }



        $styleA1 = $phpExcelObject->getActiveSheet()->getStyle('A1');
        $styleFont = $styleA1->getFont();
        $styleFont->setBold(true);
        $styleFont->setSize(12);
        $styleFont->setName('Arial');
        $styleFont->getColor()->setARGB(\PHPExcel_Style_Color::COLOR_WHITE);
        $styleA1->applyFromArray(
            array('fill' => array('type' => \PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => '1E90FF'))
            ));

        foreach(range('C','N') as $j){
            for($i=3;$i<count($tickets);$i++){
                if($phpExcelObject->getActiveSheet()->getCell($j.$i)->getValue() == 0 OR $phpExcelObject->getActiveSheet()->getCell($j.$i)->getValue() == null ){
                    $phpExcelObject->getActiveSheet()->setCellValue($j.$i,null);
                    $style = $phpExcelObject->getActiveSheet()->getStyle($j.$i);
                    $style->applyFromArray(
                        array('fill' => array('type' => \PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => '808080'))
                        ));
                }
            }
        }


        $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel5');
        // create the response
        $response = $this->get('phpexcel')->createStreamedResponse($writer);
        // adding headers
        $dispositionHeader = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            'export-ticket.xls'
        );
        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');
        $response->headers->set('Content-Disposition', $dispositionHeader);

        return $response;

        /*echo '<pre>';
        print_r($tickets);
        echo '</pre>';*/

        return $this->render('RedmineTestBundle:Default:index.html.twig');
    }

    public function unique_multidim_array($array, $key) {
        $temp_array = array();
        $i = 0;
        $key_array = array();

        foreach($array as $val) {
            if (!in_array($val[$key], $key_array)) {
                $key_array[$i] = $val[$key];
                $temp_array[$i] = $val;
            }
            $i++;
        }
        return $temp_array;
    }

    /**
     * @param $request
     * @return Response
     * @Route("/user",name="exportsby_user")
     */




    public function exportUserAction(Request $request)
    {


        // Cette Fonction retourne les tickets assignée à un Utilisateur donnée en arguments

        //$buzz = $this->container->get('buzz');
        $date_debut = $request->get('date_debut');
        $date_fin = $request->get('date_fin');
        $date_debut_reel = $request->get('date_debut_réelle');
        $date_fin_reel = $request->get('date_fin_réelle');
        $session = $this->get('session');
        $username = $session->get('name');
        $password = $session->get('pass');
        //var_dump($username.' '.$password);die();


        $url = '';
        $currentMonth = date('F');
        $last = Date('F', strtotime($currentMonth . " last month"));
        $translator = new Translator('fr_FR');
        $translator->addLoader('array', new ArrayLoader());
        $translator->addResource('array', array(
            'January' => 'Janvier',
            'February' => 'Février',
            'March' => 'Mars',
            'April' => 'Avril',
            'May' => 'Mai',
            'June' => 'Juin',
            'July' => 'Juillet',
            'August' => 'Août',
            'September' => 'Septembre',
            'October' => 'Octobre',
            'November' => 'Novembre',
            'December' => 'Décembre'

        ), 'fr_FR');

        $last_mounth = $translator->trans($last);


        if ($date_debut !== "" && $date_fin == "") {
            $url .= 'start_date=' . $date_debut;
        } elseif ($date_debut !== "" && $date_fin !== "") {
            $url .= 'start_date=%3E%3D' . $date_debut;
            $url .= '&due_date=%3C%3D' . $date_fin;
        } elseif ($date_fin !== "" && $date_debut == "") {
            $url .= 'due_date=' . $date_fin;
        }


        if ($date_debut_reel !== "") {
            if ($url !== "") {
                $url .= '&cf_10=%3E%3D' . $date_debut_reel;
            } else {
                $url .= 'cf_10=' . $date_debut_reel;
            }
        }
        if ($date_fin_reel !== "") {
            if ($url !== "") {
                $url .= '&cf_11=%3C%3D' . $date_fin_reel;
            } else {
                $url .= 'cf_11=' . $date_fin_reel;
            }

        }

        $i = 0;
        $tickets = array();
        $client = new guzzle();
        /*$project = $client->request('GET', 'http://redmine.itroad.ma/issues.json?offset=0&limit=5000&'.$url, [
            'auth' => [$username, $password]
        ]);


       // $project = $buzz->get('http://localhost/redmine/issues.json?'.$url);
        $project_content = $project->getBody();
        $result = json_decode($project_content);*/

        $issues_spent = $client->request('GET', 'http://redmine.itroad.ma/time_entries.json?utf8=✓&period_type=1&period=last_month&offset=100&limit=500', [
            'auth' => [$username, $password]
        ]);
        $issues_spent_content = $issues_spent->getBody();
        $results = json_decode($issues_spent_content);

        // le nombre de page sur Redmine
        $page_number = ($results->total_count) / 100;

        if (floatval($page_number)) {
            $page_number = round($page_number, 0) + 1;
        }



        $user = $request->get('user');
        $time_lastmount = 0;
        $total_spent_time = 0;
        //foreach($result->issues as $issue) {

        for ($k = 1; $k <= $page_number; $k++) {

        $issues_spent_time_user = $client->request('GET', 'http://redmine.itroad.ma/time_entries.json?utf8=✓&period_type=1&period=last_month&page='.$k.'&limit=100', [
            'auth' => [$username, $password]
        ]);
        $issues_spent_conetent_user = $issues_spent_time_user->getBody();
        $result_time_user = json_decode($issues_spent_conetent_user);


        foreach ($result_time_user->time_entries as $rs) {

            //var_dump(str_replace(' ', '', $rs->user->name));die();


            if (str_replace(' ', '', $rs->user->name) == str_replace(' ', '', $user)) {

                // Calculer la somme des heures passées durant les imputations
                $total_spent_time += $rs->hours;

                //$issues_spent_time = $buzz->get('http://localhost/redmine/issues/'.$issue->id.'.json');
                //$issue_spent_time_in_mount = $buzz->get('http://localhost/redmine/issues/'.$issue->id.'.json?utf8=%E2%9C%93&set_filter=1&sort=spent_on%3Adesc&f%5B%5D=spent_on&op%5Bspent_on%5D=m&f%5B%5D=&c%5B%5D=spent_on&c%5B%5D=user&c%5B%5D=activity&c%5B%5D=issue&c%5B%5D=comments&c%5B%5D=hours&group_by=&t%5B%5D=hours&t%5B%5D=');
                $issue_spent_time_in_mount = $client->request('GET', 'http://redmine.itroad.ma/time_entries.json?utf8=✓&period_type=1&period=last_month&issue_id=' . $rs->issue->id, [
                    'auth' => [$username, $password]
                ]);
                $issues_spent_content_mounth = $issue_spent_time_in_mount->getBody();
                //$issues_spent_content = $issues_spent_time->getBody();
                //$result_time = json_decode($issues_spent_content);
                $result_time_mounth = json_decode($issues_spent_content_mounth);
                $all_issues = $client->request('GET', 'http://redmine.itroad.ma/issues/' . $rs->issue->id . '.json?'.$url, [
                    'auth' => [$username, $username]
                ]);
                $issues = $all_issues->getBody();
                $issues_info = json_decode($issues);


                $project_name = $issues_info->issue->project->name;
                //$tracker_name = $issue->tracker->name;
                //$status_name  = $issue->status->name;
                //$priority_name = $issue->priority->name;
                //$subject = $issues_info->issue->subject;
                $start_date = $issues_info->issue->start_date;
                //$updated_on = $issue->updated_on;
                //$tickets[$i]['Ticket Parent'] = $issue->subject;
                $tickets[$i]['Id'] = $issues_info->issue->id;
                $tickets[$i]['Project Name'] = $project_name;
                $tickets[$i]['Subject'] = $issues_info->issue->subject;
                //$tickets[$i]['Tracker Name'] = $tracker_name;
                //$tickets[$i]['Status Name'] = $status_name;
                //$tickets[$i]['Priority Name'] = $priority_name;
                //$tickets[$i][$issues_info->issue->subject]['Subject'] = $subject;
                $tickets[$i]['Start Date'] = $start_date;

                //$tickets[$i]['Imputé le'] = $rs->spent_on;
                //$tickets[$i]['temps imputation'] = $rs->hours;

                //$tickets[$i]['Updated on'] = $updated_on;
                if (isset($issues_info->issue->estimated_hours)) {
                    $tickets[$i]['Estimaed Hours'] = $issues_info->issue->estimated_hours;
                }
                if (isset($issues_info->issue->due_date)) {
                    $tickets[$i]['Due Date'] = $issues_info->issue->due_date;

                }
                if (isset($issues_info->issue->done_ratio)) {
                    $tickets[$i]['isDone %'] = $issues_info->issue->done_ratio;

                }
                //$tickets[$i]['Spent Time'] = $result_time->issue->spent_hours;
                //$tickets[$i]['total Spent Time'] = $result_time->issue->spent_hours;

                //$total_spent_time += $rs->hours;
                // $tickets[$i]['total Spent Time'] = $total_spent_time;

                $tickets[$i]['total Spent Time'] = $issues_info->issue->spent_hours;

                foreach ($result_time_mounth->time_entries as $time) {
                    if (isset($time->hours)) {
                        $time_lastmount += $time->hours;
                    }
                }
                $tickets[$i]['total Spent Time Mount'] = $time_lastmount;

                foreach ($issues_info->issue->custom_fields as $value) {
                    if ($value->id == 10) {
                        if (isset($value->value) == "") {
                            $tickets[$i]['date_début-réelle'] = 'Non Remplie';
                        } else {
                            $tickets[$i]['date_début-réelle'] = $value->value;
                        }
                    }
                    if ($value->id == 11) {
                        if (isset($value->value) == "") {
                            $tickets[$i]['date_fin-réelle'] = 'Non Remplie';
                        } else {
                            $tickets[$i]['date_fin-réelle'] = $value->value;
                        }

                    }
                }
                if (isset($tickets[$i]['Estimaed Hours']) > 0) {
                    $tickets[$i]['depassement'] = round((($tickets[$i]['total Spent Time'] - $tickets[$i]['Estimaed Hours']) / $tickets[$i]['Estimaed Hours']) * 100, 0) . '%';

                }

                if ($tickets[$i]['date_début-réelle'] !== 'Non Remplie') {
                    if ($tickets[$i]['date_début-réelle'] == $tickets[$i]['Start Date']) {
                        $tickets[$i]['respcet_date_debut'] = 1;
                    } else {
                        $tickets[$i]['respcet_date_debut'] = 0;
                    }
                }

                if ($tickets[$i]['date_fin-réelle'] !== 'Non Remplie') {
                    if (isset($tickets[$i]['Due Date'])) {
                        if ($tickets[$i]['date_fin-réelle'] == $tickets[$i]['Due Date']) {
                            $tickets[$i]['respcet_date_fin'] = 1;
                        } else {
                            $tickets[$i]['respcet_date_fin'] = 0;
                        }

                    }
                }

                $time_lastmount = 0;
                $i++;


            }


        }

    }



            //$total_spent_time = 0;

        //}





        //$tickets = array_unique($tickets,SORT_REGULAR);

        /*echo '<pre>';
        print_r($tickets);
        echo '</pre>';die();*/

        //$tickets = array_values($tickets);

        $tickets = $this->unique_multidim_array($tickets,'Id');

        $tickets = array_values($tickets);

        /*echo '<pre>';
        print_r($tickets);
        echo '</pre>';die();*/

        //session_destroy();


       for($j=0;$j<count($tickets);$j++) {
               $expallticket = new ExportTicketByUser();
               $expallticket->setUser($user);
               $expallticket->setNomProjet($tickets[$j]['Project Name'])
                   ->setTicket($tickets[$j]['Subject']);
               if (isset($tickets[$j]['Estimaed Hours'])) {
                   $expallticket->setTempsEstimee($tickets[$j]['Estimaed Hours']);
               }

               if (isset($tickets[$j]['Due Date'])) {
                   $expallticket->setDateFinPlannifiee(new \DateTime($tickets[$j]['Due Date']));
               }

               if (isset($tickets[$j]['Start Date'])) {
                   $expallticket->setDateDebutPlannifiee(new \DateTime($tickets[$j]['Start Date']));
               }
               $expallticket
                   ->setAvancement($tickets[$j]['isDone %']);

               if ($tickets[$j]['date_début-réelle'] == 'Non Remplie') {
                   $expallticket->setDateDebutReelle(null);
               } else {
                   $expallticket->setDateDebutReelle(new \DateTime($tickets[$j]['date_début-réelle']));

               }


               if ($tickets[$j]['date_fin-réelle'] == 'Non Remplie') {
                   $expallticket->setDateFinReelle(null);
               } else {
                   $expallticket->setDateFinReelle(new \DateTime($tickets[$j]['date_fin-réelle']));

               }

               $expallticket->setTotalTempsPasse($tickets[$j]['total Spent Time'])
                   ->setTotalTempsPasseMois($tickets[$j]['total Spent Time Mount']);
               if (isset($tickets[$j]['depassement'])) {
                   $expallticket->setDepacement($tickets[$j]['depassement']);
               }

               if (isset($tickets[$j]['respcet_date_debut'])) {
                   $expallticket->setRespectDateDebut($tickets[$j]['respcet_date_debut']);
               } else {
                   $expallticket->setRespectDateDebut(null);
               }

               if (isset($tickets[$j]['respcet_date_fin'])) {
                   $expallticket->setRespectDateFin($tickets[$j]['respcet_date_fin']);
               } else {
                   $expallticket->setRespectDateFin(null);
               }


               $em = $this->getDoctrine()->getManager();
               $em->persist($expallticket);
               $em->flush();

       }


        $k = 3;
        $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();
        $styleArray = array(
            'borders' => array(
                'allborders' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );
        $phpExcelObject->getDefaultStyle()->applyFromArray($styleArray);
        $phpExcelObject->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Ressource')
            ->setCellValue('A2','Nom Projet')
            //->setCellValue('B2', 'Type Ticket')
            //->setCellValue('C2', 'Status')
            //->setCellValue('D2',' Priotité')
            ->setCellValue('B2','Ticket')
            ->setCellValue('C2','Date Debut Plannifiée')
            //->setCellValue('G2','Date Mise à jour')
            ->setCellValue('D2','Temps Estimé')
            ->setCellValue('E2','Date fin Plannifiée')
            ->setCellValue('F2','Avancement')
            ->setCellValue('G2','Date Début Réelle')
            ->setCellValue('H2','Date Fin Réelle')
            //->setCellValue('M2','Temps Passé')
            ->setCellValue('I2','Total Temps Passé')
            ->setCellValue('J2','Total Temps Passé ('.$last_mounth.')')
            ->setCellValue('K2','Dépassement')
            ->setCellValue('L2','Respect Date Debut')
            ->setCellValue('M2','Respect Date Fin');


        $phpExcelObject->getActiveSheet()->mergeCells('A1:B1');
        $phpExcelObject->getActiveSheet()->mergeCells('B1:C1');
        $phpExcelObject->getActiveSheet()->mergeCells('C1:D1');
        $phpExcelObject->getActiveSheet()->mergeCells('D1:E1');
        $phpExcelObject->getActiveSheet()->mergeCells('E1:F1');
        $phpExcelObject->getActiveSheet()->mergeCells('F1:G1');
        $phpExcelObject->getActiveSheet()->mergeCells('G1:H1');
        $phpExcelObject->getActiveSheet()->mergeCells('H1:I1');
        $phpExcelObject->getActiveSheet()->mergeCells('I1:J1');
        $phpExcelObject->getActiveSheet()->mergeCells('J1:K1');
        $phpExcelObject->getActiveSheet()->mergeCells('J1:L1');
        $phpExcelObject->getActiveSheet()->mergeCells('L1:M1');



        /*----------------------------------------------------------------------------------------------*/


        for($i=0;$i<count($tickets);$i++) {
                $phpExcelObject->setActiveSheetIndex(0)
                    ->setCellValue('A' . $k, $tickets[$i]['Project Name'])
                    //->setCellValue('B'.$k, $tickets[$i]['Tracker Name'])
                    //->setCellValue('C'.$k, $tickets[$i]['Status Name'])
                    //->setCellValue('D'.$k,$tickets[$i]['Priority Name'])
                    ->setCellValue('B' . $k, $tickets[$i]['Subject'])
                    ->setCellValue('C' . $k, $tickets[$i]['Start Date']);
                //->setCellValue('G'.$k,$tickets[$i]['Updated on'])
                if (isset($tickets[$i]['Estimaed Hours'])) {
                    $phpExcelObject->setActiveSheetIndex(0)->setCellValue('D' . $k, $tickets[$i]['Estimaed Hours']);
                }
                if (isset($tickets[$i]['Due Date'])) {
                    $phpExcelObject->setActiveSheetIndex(0)->setCellValue('E' . $k, $tickets[$i]['Due Date']);

                }
                $phpExcelObject->setActiveSheetIndex(0)
                    ->setCellValue('F' . $k, $tickets[$i]['isDone %'] . '%')
                    ->setCellValue('G' . $k, $tickets[$i]['date_début-réelle'])
                    ->setCellValue('H' . $k, $tickets[$i]['date_fin-réelle'])
                    //->setCellValue('M'.$k,$tickets[$i]['Spent Time'])
                    ->setCellValue('I' . $k, $tickets[$i]['total Spent Time'])
                    ->setCellValue('J' . $k, $tickets[$i]['total Spent Time Mount']);
                if (isset($tickets[$i]['depassement'])) {
                    $phpExcelObject->setActiveSheetIndex(0)->setCellValue('K' . $k, $tickets[$i]['depassement']);
                }

                $styleP = $phpExcelObject->getActiveSheet()->getStyle('K' . $k);
                $styleP->applyFromArray(
                    array('fill' => array('type' => \PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => 'FA8072'))
                    ));
                if (isset($tickets[$i]['respcet_date_debut'])) {
                    $phpExcelObject->setActiveSheetIndex(0)->setCellValue('L' . $k, $tickets[$i]['respcet_date_debut']);
                    if ($tickets[$i]['respcet_date_debut'] == 0) {
                        $styleR = $phpExcelObject->getActiveSheet()->getStyle('L' . $k);
                        $styleR->applyFromArray(
                            array('fill' => array('type' => \PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => '98FB98'))
                            ));
                    } else {
                        $styleR = $phpExcelObject->getActiveSheet()->getStyle('L' . $k);
                        $styleR->applyFromArray(
                            array('fill' => array('type' => \PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => '008000'))
                            ));
                    }
                }
                if (isset($tickets[$i]['respcet_date_fin'])) {
                    $phpExcelObject->setActiveSheetIndex(0)->setCellValue('M' . $k, $tickets[$i]['respcet_date_fin']);
                    if ($tickets[$i]['respcet_date_fin'] == 0) {
                        $styleR = $phpExcelObject->getActiveSheet()->getStyle('M' . $k);
                        $styleR->applyFromArray(
                            array('fill' => array('type' => \PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => '98FB98'))
                            ));
                    } else {
                        $styleR = $phpExcelObject->getActiveSheet()->getStyle('M' . $k);
                        $styleR->applyFromArray(
                            array('fill' => array('type' => \PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => '008000'))
                            ));
                    }
                }
                $k = $k + 1;

        }


        $phpExcelObject->getActiveSheet()->getColumnDimension('A')->setWidth(20);
        $phpExcelObject->getActiveSheet()->getColumnDimension('B')->setWidth(40);
        $phpExcelObject->getActiveSheet()->getColumnDimension('C')->setWidth(25);
        $phpExcelObject->getActiveSheet()->getColumnDimension('D')->setWidth(15);
        $phpExcelObject->getActiveSheet()->getColumnDimension('E')->setWidth(25);
        $phpExcelObject->getActiveSheet()->getColumnDimension('F')->setWidth(20);
        $phpExcelObject->getActiveSheet()->getColumnDimension('G')->setWidth(25);
        $phpExcelObject->getActiveSheet()->getColumnDimension('H')->setWidth(25);
        $phpExcelObject->getActiveSheet()->getColumnDimension('I')->setWidth(25);
        $phpExcelObject->getActiveSheet()->getColumnDimension('J')->setWidth(25);
        $phpExcelObject->getActiveSheet()->getColumnDimension('K')->setWidth(15);
        $phpExcelObject->getActiveSheet()->getColumnDimension('L')->setWidth(25);
        $phpExcelObject->getActiveSheet()->getColumnDimension('M')->setWidth(25);


        foreach(range('A','M') as $i) {
            $style = $phpExcelObject->getActiveSheet()->getStyle($i.'2');
            $styleFont = $style->getFont();
            $styleFont->setBold(true);
            $styleFont->setSize(10);
            $styleFont->setName('Arial');
        }



        $styleA1 = $phpExcelObject->getActiveSheet()->getStyle('A1');
        $styleFont = $styleA1->getFont();
        $styleFont->setBold(true);
        $styleFont->setSize(12);
        $styleFont->setName('Arial');
        $styleFont->getColor()->setARGB(\PHPExcel_Style_Color::COLOR_WHITE);
        $styleA1->applyFromArray(
            array('fill' => array('type' => \PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => '1E90FF'))
            ));

        foreach(range('C','M') as $j){
            for($i=3;$i<count($tickets);$i++){
                if($phpExcelObject->getActiveSheet()->getCell($j.$i)->getValue() == null ){
                    $phpExcelObject->getActiveSheet()->setCellValue($j.$i,null);
                    $style = $phpExcelObject->getActiveSheet()->getStyle($j.$i);
                    $style->applyFromArray(
                        array('fill' => array('type' => \PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => '808080'))
                        ));
                }
            }
        }


        $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel5');
        // create the response
        $response = $this->get('phpexcel')->createStreamedResponse($writer);
        // adding headers
        $dispositionHeader = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $user.'-'.$last_mounth.'-'.'export.xls'
        );
        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');
        $response->headers->set('Content-Disposition', $dispositionHeader);

        return $response;


        /*echo '<pre>';
        print_r($tickets);
        echo '</pre>';*/


        }

}
