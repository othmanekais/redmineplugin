<?php
/**
 * Created by PhpStorm.
 * User: Othmanekais
 * Date: 23/03/2018
 * Time: 15:53
 */

namespace Redmine\TestBundle\Controller;



use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Redmine\TestBundle\Entity\allTicketsExports;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class ReportsController extends Controller
{

    /**
     * @return Response
     * @Route("/reports/all",name="get_reports_all")
     */
    function getAllTicketsReportsAction(){
        $repisotory = $this->getDoctrine()
            ->getRepository('RedmineTestBundle:allTicketsExports');

        $alltickets = $repisotory->findAll();
        $i = 0;

        /*echo '<pre>';
        print_r($alltickets);
        echo '</pre>'; die();*/
        $listProject = array();
       foreach ($alltickets as $value){

           $listProject[$value->getNomProjet()][$value->getTicket()]['Temps Estimé'] = $value->getTempsEstime() ;
           $listProject[$value->getNomProjet()][$value->getTicket()]['Temps Passé réel mois'] = $value->getTotalTempsPasseMois() ;
           $listProject[$value->getNomProjet()][$value->getTicket()]['Respect Date Debut'] = $value->getRespectDateDebut();
           $listProject[$value->getNomProjet()][$value->getTicket()]['Respect Date fin'] = $value->getRespectDateFin();


           //$listProject[$value->getNomProjet()][$i]['Ticket'] = $value->getTicket() ;
          // $listProject[$value->getNomProjet()][$i]['Temps Estimé'] = $value->getTempsEstime() ;
           //$listProject[$value->getNomProjet()][$i]['Temps Passé réel mois'] = $value->getTotalTempsPasseMois() ;

            $i++;

           //if(!in_array(array($value->getTicket(), $value->getNomProjet()),$ticket)){
               //   $ticket[$i]['Ticket'] = $value->getTicket();
               //   $ticket[$i]['Projet'] = $value->getNomProjet();

               //   $i++;
           //}ç

       }




        /*echo '<pre>';
        print_r($listProject);
        echo '</pre>';die();*/



        // Vider les tables aprés la phase reporting

        $em = $this->getDoctrine()->getManager();
        $connection = $em->getConnection();
        $platform   = $connection->getDatabasePlatform();

        $connection->executeUpdate($platform->getTruncateTableSQL('all_tickets_exports', true ));



        $k = 3;
        $v = 3;
        $sommeVR = 0;
        $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();
        $styleArray = array(
            'borders' => array(
                'allborders' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );
        $phpExcelObject->getDefaultStyle()->applyFromArray($styleArray);
        $phpExcelObject->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Ventilation de la charge')
            ->setCellValue('A2','Nom Projet')
            ->setCellValue('B2','Ticket')
            ->setCellValue('C2','plannifié')
            ->setCellValue('D2','consommée');

        $phpExcelObject->getActiveSheet()->mergeCells('A1:B1');
        $phpExcelObject->getActiveSheet()->mergeCells('B1:C1');
        $phpExcelObject->getActiveSheet()->mergeCells('C1:D1');

        foreach ($listProject as $project => $data_project){
            $phpExcelObject->setActiveSheetIndex(0)
                ->setCellValue('A'.$v, $project);
            if($data_project){
                foreach ($data_project as $ticket => $data_ticket){

                    $phpExcelObject->setActiveSheetIndex(0)
                        ->setCellValue('B'.$k, $ticket)
                        ->setCellValue('C'.$k,$data_ticket['Temps Estimé'] / 8)
                        ->setCellValue('D'.$k,$data_ticket['Temps Passé réel mois'] / 8);
                    $k = $k + 1;
                    $v = $v + 1;
                }

            }else{
                $v++;
            }


        }


        $f = $k +2 ;

        $phpExcelObject->setActiveSheetIndex(0)
            ->setCellValue('B'.$f , 'Calcul des Taux');


            $style = $phpExcelObject->getActiveSheet()->getStyle('B'.$f);
            $styleFont = $style->getFont();
            $styleFont->setBold(true);
             $styleFont->getColor()->setARGB(\PHPExcel_Style_Color::COLOR_WHITE);
            $styleFont->setSize(10);
            $styleFont->setName('Arial');
            $style->applyFromArray(
                array('fill' => array('type' => \PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => '32CD32'))
                ));
            $d = $f+1;
        $phpExcelObject->setActiveSheetIndex(0)
            ->setCellValue('A'.$d, 'Velocité (hj)')
            ->setCellValue('B'.$d,'Velocité Réelle (hj)')
            ->setCellValue('C'.$d,'Velocité consommée (hj)')
            ->setCellValue('D'.$d,'% Imputation')
            ->setCellValue('E'.$d,'% Respect Date début')
            ->setCellValue('F'.$d,'% Respect Date Fin')
            ->setCellValue('G'.$d,'% depassement de plan de charge plannifié')
            ->setCellValue('H'.$d,'% Respect de plan de charge')
            ->setCellValue('I'.$d,'% de productivité');

        foreach(range('A','I') as $i) {
            $style = $phpExcelObject->getActiveSheet()->getStyle($i.$d);
            $styleFont = $style->getFont();
            $styleFont->setBold(true);
            $styleFont->getColor()->setARGB(\PHPExcel_Style_Color::COLOR_WHITE);
            $styleFont->setSize(8);
            $styleFont->setName('Arial');
            $style->applyFromArray(
                array('fill' => array('type' => \PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => '363636'))
                ));
        }

        $r = $d + 1;
        $n = $d + 1;
        $sommeP = 0;
        $sommeC = 0;
        $sommeImput = 0;
        $sommeRDD = 0;
        $sommeRDF = 0;
        foreach ($listProject as $project => $data_project){
            $phpExcelObject->setActiveSheetIndex(0)
                ->setCellValue('j'.$r, $project);
            $style = $phpExcelObject->getActiveSheet()->getStyle('j'.$r);
            $style->applyFromArray(
                array('fill' => array('type' => \PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => '7FFFD4'))
                ));
            if($data_project){
                foreach ($data_project as $ticket => $data_ticket){
                    $vel = $data_ticket['Temps Estimé'] / 8;
                    $vel_consom = $data_ticket['Temps Passé réel mois'] / 8;
                    $respect_date_debut = $data_ticket['Respect Date Debut'];
                    $respect_date_fin = $data_ticket['Respect Date fin'];
                    $sommeP += $vel;
                    $sommeC += $vel_consom;
                    $sommeImput += $vel_consom;
                    if($vel !== 0 AND $vel_consom !== 0){
                        $sommeVR += $vel;
                    }
                    //var_dump();die();

                    if(isset($respect_date_debut)){
                        $sommeRDD += $respect_date_debut;
                    }

                    if(isset($respect_date_fin)){
                        $sommeRDF += $respect_date_fin;
                    }
                }

                $phpExcelObject->setActiveSheetIndex(0)
                    ->setCellValue('A'.$r, $sommeP)
                    ->setCellValue('B'.$r,$sommeVR)
                    ->setCellValue('C'.$r,$sommeC)
                    ->setCellValue('E'.$r,($sommeRDD/sizeof($data_project)) * 100 .'%')
                    ->setCellValue('F'.$r,($sommeRDF/sizeof($data_project)) * 100 .'%')
                    ->setCellValue('G'.$r,round((($sommeC - $sommeP) / $sommeP ) * 100 ,0).'%' )
                    ->setCellValue('H'.$r,$sommeP + $sommeVR);
                if($sommeC !== 0){
                    $phpExcelObject->setActiveSheetIndex(0)
                        ->setCellValue('I'.$r,round(($sommeVR / $sommeC) ,0).'%');
                } else {
                    $phpExcelObject->setActiveSheetIndex(0)
                        ->setCellValue('I'.$r,'VC est null');
                }

                if($sommeC !== 0 AND $sommeVR < $sommeC){
                    $phpExcelObject->setActiveSheetIndex(0)
                        ->setCellValue('I'.$r,round(($sommeVR / $sommeC) * 100 ,0).'%');
                }


                $r = $r +1 ;
                $sommeP = 0;
                $sommeC = 0;
                $sommeVR = 0;
                $sommeRDD = 0;
                $sommeRDF = 0;
            }
        }
        $phpExcelObject->setActiveSheetIndex(0)->setCellValue('D'.$n,round(($sommeImput / 21) * 100 ,2). '%');
        $e = $r + 2;
        $phpExcelObject->setActiveSheetIndex(0)
            ->setCellValue('B'.$e , 'Taux du respect du plan de charge');


        $style = $phpExcelObject->getActiveSheet()->getStyle('B'.$e);
        $styleFont = $style->getFont();
        $styleFont->setBold(true);
        $styleFont->getColor()->setARGB(\PHPExcel_Style_Color::COLOR_WHITE);
        $styleFont->setSize(9);
        $styleFont->setName('Arial');
        $style->applyFromArray(
            array('fill' => array('type' => \PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => '32CD32'))
            ));

        $t = $e + 2;
        $decale_line = $t + 1;
        $decale_line1 = $t + 1;
        $sommePlan = 0;
        $sommeConsom = 0;
        $sommeVelo_reelle = 0;
        $ticketPNT = 0;
        $ticketTNP = 0;

        $phpExcelObject->setActiveSheetIndex(0)
            ->setCellValue('B'.$t,'Durée total de tickets planifiés (h)')
            ->setCellValue('C'.$t,'Tickets planifiés traités (h)')
            ->setCellValue('D'.$t,'Tickets planifiés non traités (h)')
            ->setCellValue('E'.$t,'Tickets traités non planifiés (h)')
            ->setCellValue('F'.$t,'Taux du respect du plan de charge ');

        foreach(range('B','F') as $i) {
            $style = $phpExcelObject->getActiveSheet()->getStyle($i . $t);
            $styleFont = $style->getFont();
            $styleFont->setBold(true);
            $styleFont->getColor()->setARGB(\PHPExcel_Style_Color::COLOR_WHITE);
            $styleFont->setSize(8);
            $styleFont->setName('Arial');
            $style->applyFromArray(
                array('fill' => array('type' => \PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => '363636'))
                ));

        }

        foreach ($listProject as $project => $data_project){
            $phpExcelObject->setActiveSheetIndex(0)
                ->setCellValue('A'.$decale_line, $project);
            $style = $phpExcelObject->getActiveSheet()->getStyle('A'.$decale_line);
            $style->applyFromArray(
                array('fill' => array('type' => \PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => '7FFFD4'))
                ));
            if($data_project){
                foreach ($data_project as $ticket => $data_ticket){
                    $vel = $data_ticket['Temps Estimé'] / 8;
                    $vel_consom = $data_ticket['Temps Passé réel mois'] / 8;
                    $sommePlan += $vel;
                    if($vel !== 0 AND $vel_consom !== 0){
                        $sommeVelo_reelle += $vel;
                    }
                    if($vel !== 0 AND $vel_consom == 0){
                        $ticketPNT += $vel;
                    }

                    if($vel == 0 AND $vel_consom !== 0){
                        $ticketTNP += $vel_consom;
                    }
                    //var_dump();die();
                }

                $phpExcelObject->setActiveSheetIndex(0)
                    ->setCellValue('B'.$decale_line1, $sommePlan)
                    ->setCellValue('C'.$decale_line1,$sommeVelo_reelle)
                    ->setCellValue('D'.$decale_line1,$ticketPNT)
                    ->setCellValue('E'.$decale_line1,$ticketTNP)
                    ->setCellValue('F'.$decale_line1,$sommePlan + $sommeVelo_reelle);



                $decale_line1++;
                $sommePlan = 0;
                $sommeVelo_reelle = 0;
                $ticketPNT = 0; // Ticket Plannifiées non traités
                $ticketTNP = 0; // Ticket Traitées non plannifiées
            }
            $decale_line++;

        }


        $phpExcelObject->getActiveSheet()->getColumnDimension('A')->setWidth(15);
        $phpExcelObject->getActiveSheet()->getColumnDimension('B')->setWidth(40);
        $phpExcelObject->getActiveSheet()->getColumnDimension('C')->setWidth(22);
        $phpExcelObject->getActiveSheet()->getColumnDimension('D')->setWidth(22);
        $phpExcelObject->getActiveSheet()->getColumnDimension('E')->setWidth(22);
        $phpExcelObject->getActiveSheet()->getColumnDimension('F')->setWidth(25);
        $phpExcelObject->getActiveSheet()->getColumnDimension('G')->setWidth(40);
        $phpExcelObject->getActiveSheet()->getColumnDimension('H')->setWidth(22);
        $phpExcelObject->getActiveSheet()->getColumnDimension('I')->setWidth(15);
        $phpExcelObject->getActiveSheet()->getColumnDimension('J')->setWidth(15);

        foreach(range('A','D') as $i) {
            $style = $phpExcelObject->getActiveSheet()->getStyle($i.'2');
            $styleFont = $style->getFont();
            $styleFont->setBold(true);
            $styleFont->setSize(10);
            $styleFont->setName('Arial');
            $style->applyFromArray(
                array('fill' => array('type' => \PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => '00BFFF'))
                ));
        }

        $styleA1 = $phpExcelObject->getActiveSheet()->getStyle('A1');
        $styleFont = $styleA1->getFont();
        $styleFont->setBold(true);
        $styleFont->setSize(10);
        $styleFont->setName('Arial');
        $styleFont->getColor()->setARGB(\PHPExcel_Style_Color::COLOR_WHITE);
        $styleA1->applyFromArray(
            array('fill' => array('type' => \PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => '32CD32'))
            ));

        $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel5');
        // create the response
        $response = $this->get('phpexcel')->createStreamedResponse($writer);
        // adding headers
        $dispositionHeader = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            'reporting.xls'
        );
        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');
        $response->headers->set('Content-Disposition', $dispositionHeader);

        return $response;
        return $this->render('RedmineTestBundle:Default:index.html.twig');

    }

    /**
     * @Route("/reports/by_project", name="get_reports_project")
     * @return Response
     */

    function getReportsByProjectAction(){
        $repisitory = $this->getDoctrine()->getRepository('RedmineTestBundle:ExportTicketByProject');
        $alltickets = $repisitory->findAll();
        $listproject = array();


        /*echo '<pre>';
        print_r($alltickets);
        echo '</pre>';die();*/

        foreach ($alltickets as $value){
            $listproject[$value->getNomProjet()][$value->getTicket()]['Temps Estimé'] = $value->getTempsEstime();
            $listproject[$value->getNomProjet()][$value->getTicket()]['Totale Temps Passé Mois'] = $value->getTotaleTempsPasseMois();
            $listproject[$value->getNomProjet()][$value->getTicket()]['Respect Date Début'] = $value->getRespectDateDebut();
            $listproject[$value->getNomProjet()][$value->getTicket()]['Respect Date Fin'] = $value->getRespectDateFin();
        }

        $k = 3;
        $v = 3;
        $sommeVR = 0;
        $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();
        $styleArray = array(
            'borders' => array(
                'allborders' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );
        $phpExcelObject->getDefaultStyle()->applyFromArray($styleArray);
        $phpExcelObject->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Ventilation de la charge')
            ->setCellValue('A2','Nom Projet')
            ->setCellValue('B2','Ticket')
            ->setCellValue('C2','plannifié')
            ->setCellValue('D2','consommée');

        $phpExcelObject->getActiveSheet()->mergeCells('A1:B1');
        $phpExcelObject->getActiveSheet()->mergeCells('B1:C1');
        $phpExcelObject->getActiveSheet()->mergeCells('C1:D1');

        foreach ($listproject as $project => $data_project){
            $phpExcelObject->setActiveSheetIndex(0)
                ->setCellValue('A'.$v, $project);
            if($data_project){
                foreach ($data_project as $ticket => $data_ticket){

                    $phpExcelObject->setActiveSheetIndex(0)
                        ->setCellValue('B'.$k, $ticket)
                        ->setCellValue('C'.$k,$data_ticket['Temps Estimé'] / 8)
                        ->setCellValue('D'.$k,$data_ticket['Totale Temps Passé Mois'] / 8);
                    $k = $k + 1;
                    $v = $v + 1;
                }

            }else{
                $v++;
            }

            $f = $k +2 ;

            $phpExcelObject->setActiveSheetIndex(0)
                ->setCellValue('B'.$f , 'Calcul des Taux');


            $style = $phpExcelObject->getActiveSheet()->getStyle('B'.$f);
            $styleFont = $style->getFont();
            $styleFont->setBold(true);
            $styleFont->getColor()->setARGB(\PHPExcel_Style_Color::COLOR_WHITE);
            $styleFont->setSize(10);
            $styleFont->setName('Arial');
            $style->applyFromArray(
                array('fill' => array('type' => \PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => '32CD32'))
                ));
            $d = $f+1;
            $phpExcelObject->setActiveSheetIndex(0)
                ->setCellValue('A'.$d, 'Velocité (hj)')
                ->setCellValue('B'.$d,'Velocité Réelle (hj)')
                ->setCellValue('C'.$d,'Velocité consommée (hj)')
                ->setCellValue('D'.$d,'% Imputation')
                ->setCellValue('E'.$d,'% Respect Date début')
                ->setCellValue('F'.$d,'% Respect Date Fin')
                ->setCellValue('G'.$d,'% depassement de plan de charge plannifié')
                ->setCellValue('H'.$d,'% Respect de plan de charge')
                ->setCellValue('I'.$d,'% de productivité');

            foreach(range('A','I') as $i) {
                $style = $phpExcelObject->getActiveSheet()->getStyle($i.$d);
                $styleFont = $style->getFont();
                $styleFont->setBold(true);
                $styleFont->getColor()->setARGB(\PHPExcel_Style_Color::COLOR_WHITE);
                $styleFont->setSize(8);
                $styleFont->setName('Arial');
                $style->applyFromArray(
                    array('fill' => array('type' => \PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => '363636'))
                    ));
            }

            $r = $d + 1;
            $n = $d + 1;
            $sommeP = 0;
            $sommeC = 0;
            $sommeImput = 0;
            $sommeRDD = 0;
            $sommeRDF = 0;
            foreach ($listproject as $project => $data_project){
                $phpExcelObject->setActiveSheetIndex(0)
                    ->setCellValue('j'.$r, $project);
                $style = $phpExcelObject->getActiveSheet()->getStyle('j'.$r);
                $style->applyFromArray(
                    array('fill' => array('type' => \PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => '7FFFD4'))
                    ));
                if($data_project){
                    foreach ($data_project as $ticket => $data_ticket){
                        $vel = $data_ticket['Temps Estimé'] / 8;
                        $vel_consom = $data_ticket['Totale Temps Passé Mois'] / 8;
                        $respect_date_debut = $data_ticket['Respect Date Début'];
                        $respect_date_fin = $data_ticket['Respect Date Fin'];
                        $sommeP += $vel;
                        $sommeC += $vel_consom;
                        $sommeImput += $vel_consom;
                        if($vel !== 0 AND $vel_consom !== 0){
                            $sommeVR += $vel;
                        }
                        //var_dump();die();

                        if(isset($respect_date_debut)){
                            $sommeRDD += $respect_date_debut;
                        }

                        if(isset($respect_date_fin)){
                            $sommeRDF += $respect_date_fin;
                        }
                    }

                    $phpExcelObject->setActiveSheetIndex(0)
                        ->setCellValue('A'.$r, $sommeP)
                        ->setCellValue('B'.$r,$sommeVR)
                        ->setCellValue('C'.$r,$sommeC)
                        ->setCellValue('E'.$r,($sommeRDD/sizeof($data_project)) * 100 .'%')
                        ->setCellValue('F'.$r,($sommeRDF/sizeof($data_project)) * 100 .'%')
                        ->setCellValue('G'.$r,round((($sommeC - $sommeP) / $sommeP ) * 100 ,0).'%' )
                        ->setCellValue('H'.$r,$sommeP + $sommeVR);
                    if($sommeC !== 0){
                        $phpExcelObject->setActiveSheetIndex(0)
                            ->setCellValue('I'.$r,round(($sommeVR / $sommeC) ,0).'%');
                    } else {
                        $phpExcelObject->setActiveSheetIndex(0)
                            ->setCellValue('I'.$r,'VC est null');
                    }

                    if($sommeC !== 0 AND $sommeVR < $sommeC){
                        $phpExcelObject->setActiveSheetIndex(0)
                            ->setCellValue('I'.$r,round(($sommeVR / $sommeC) * 100 ,0).'%');
                    }


                    $r = $r +1 ;
                    $sommeP = 0;
                    $sommeC = 0;
                    $sommeVR = 0;
                    $sommeRDD = 0;
                    $sommeRDF = 0;
                }
            }
            $phpExcelObject->setActiveSheetIndex(0)->setCellValue('D'.$n,round(($sommeImput / 21) * 100 ,2). '%');
            $e = $r + 2;
            $phpExcelObject->setActiveSheetIndex(0)
                ->setCellValue('B'.$e , 'Taux du respect du plan de charge');


            $style = $phpExcelObject->getActiveSheet()->getStyle('B'.$e);
            $styleFont = $style->getFont();
            $styleFont->setBold(true);
            $styleFont->getColor()->setARGB(\PHPExcel_Style_Color::COLOR_WHITE);
            $styleFont->setSize(9);
            $styleFont->setName('Arial');
            $style->applyFromArray(
                array('fill' => array('type' => \PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => '32CD32'))
                ));

            $t = $e + 2;
            $decale_line = $t + 1;
            $decale_line1 = $t + 1;
            $sommePlan = 0;
            $sommeConsom = 0;
            $sommeVelo_reelle = 0;
            $ticketPNT = 0;
            $ticketTNP = 0;

            $phpExcelObject->setActiveSheetIndex(0)
                ->setCellValue('B'.$t,'Durée total de tickets planifiés (h)')
                ->setCellValue('C'.$t,'Tickets planifiés traités (h)')
                ->setCellValue('D'.$t,'Tickets planifiés non traités (h)')
                ->setCellValue('E'.$t,'Tickets traités non planifiés (h)')
                ->setCellValue('F'.$t,'Taux du respect du plan de charge ');

            foreach(range('B','F') as $i) {
                $style = $phpExcelObject->getActiveSheet()->getStyle($i . $t);
                $styleFont = $style->getFont();
                $styleFont->setBold(true);
                $styleFont->getColor()->setARGB(\PHPExcel_Style_Color::COLOR_WHITE);
                $styleFont->setSize(8);
                $styleFont->setName('Arial');
                $style->applyFromArray(
                    array('fill' => array('type' => \PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => '363636'))
                    ));

            }

            foreach ($listproject as $project => $data_project){
                $phpExcelObject->setActiveSheetIndex(0)
                    ->setCellValue('A'.$decale_line, $project);
                $style = $phpExcelObject->getActiveSheet()->getStyle('A'.$decale_line);
                $style->applyFromArray(
                    array('fill' => array('type' => \PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => '7FFFD4'))
                    ));
                if($data_project){
                    foreach ($data_project as $ticket => $data_ticket){
                        $vel = $data_ticket['Temps Estimé'] / 8;
                        $vel_consom = $data_ticket['Totale Temps Passé Mois'] / 8;
                        $sommePlan += $vel;
                        if($vel !== 0 AND $vel_consom !== 0){
                            $sommeVelo_reelle += $vel;
                        }
                        if($vel !== 0 AND $vel_consom == 0){
                            $ticketPNT += $vel;
                        }

                        if($vel == 0 AND $vel_consom !== 0){
                            $ticketTNP += $vel_consom;
                        }
                        //var_dump();die();
                    }

                    $phpExcelObject->setActiveSheetIndex(0)
                        ->setCellValue('B'.$decale_line1, $sommePlan)
                        ->setCellValue('C'.$decale_line1,$sommeVelo_reelle)
                        ->setCellValue('D'.$decale_line1,$ticketPNT)
                        ->setCellValue('E'.$decale_line1,$ticketTNP)
                        ->setCellValue('F'.$decale_line1,$sommePlan + $sommeVelo_reelle);



                    $decale_line1++;
                    $sommePlan = 0;
                    $sommeVelo_reelle = 0;
                    $ticketPNT = 0; // Ticket Plannifiées non traités
                    $ticketTNP = 0; // Ticket Traitées non plannifiées
                }
                $decale_line++;

            }
        }



        $phpExcelObject->getActiveSheet()->getColumnDimension('A')->setWidth(15);
        $phpExcelObject->getActiveSheet()->getColumnDimension('B')->setWidth(40);
        $phpExcelObject->getActiveSheet()->getColumnDimension('C')->setWidth(22);
        $phpExcelObject->getActiveSheet()->getColumnDimension('D')->setWidth(22);
        $phpExcelObject->getActiveSheet()->getColumnDimension('E')->setWidth(22);
        $phpExcelObject->getActiveSheet()->getColumnDimension('F')->setWidth(25);
        $phpExcelObject->getActiveSheet()->getColumnDimension('G')->setWidth(40);
        $phpExcelObject->getActiveSheet()->getColumnDimension('H')->setWidth(22);
        $phpExcelObject->getActiveSheet()->getColumnDimension('I')->setWidth(15);
        $phpExcelObject->getActiveSheet()->getColumnDimension('J')->setWidth(15);

        foreach(range('A','D') as $i) {
            $style = $phpExcelObject->getActiveSheet()->getStyle($i.'2');
            $styleFont = $style->getFont();
            $styleFont->setBold(true);
            $styleFont->setSize(10);
            $styleFont->setName('Arial');
            $style->applyFromArray(
                array('fill' => array('type' => \PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => '00BFFF'))
                ));
        }

        $styleA1 = $phpExcelObject->getActiveSheet()->getStyle('A1');
        $styleFont = $styleA1->getFont();
        $styleFont->setBold(true);
        $styleFont->setSize(10);
        $styleFont->setName('Arial');
        $styleFont->getColor()->setARGB(\PHPExcel_Style_Color::COLOR_WHITE);
        $styleA1->applyFromArray(
            array('fill' => array('type' => \PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => '32CD32'))
            ));

        $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel5');
        // create the response
        $response = $this->get('phpexcel')->createStreamedResponse($writer);
        // adding headers
        $dispositionHeader = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            'reporting.xls'
        );
        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');
        $response->headers->set('Content-Disposition', $dispositionHeader);

        return $response;
        return $this->render('RedmineTestBundle:Default:index.html.twig');


        /*echo '<pre>';
        print_r($listproject);
        echo '</pre>';die();*/

    }

    /**
     * @Route("/reports/by_user",name="user_reporting")
     */
    function getReportsByUserAction(){

            $repisitory = $this->getDoctrine()->getRepository('RedmineTestBundle:ExportTicketByUser');
            $alltickets = $repisitory->findAll();
            $listProject = array();

            /*echo '<pre>';
            print_r($alltickets);
            echo '</pre>';die();*/

            foreach($alltickets as $value){
                $listProject[$value->getNomProjet()][$value->getTicket()]['Temps Estimé'] = $value->getTempsEstimee() ;
                $listProject[$value->getNomProjet()][$value->getTicket()]['Temps Passé réel mois'] = $value->getTotalTempsPasseMois() ;
                $listProject[$value->getNomProjet()][$value->getTicket()]['Respect Date Debut'] = $value->getRespectDateDebut();
                $listProject[$value->getNomProjet()][$value->getTicket()]['Respect Date fin'] = $value->getRespectDateFin();
            }

            //vider les tables aprés la phase reporting

            $em = $this->getDoctrine()->getManager();
            $connection = $em->getConnection();
            $platform   = $connection->getDatabasePlatform();
            $connection->executeUpdate($platform->getTruncateTableSQL('export_ticket_by_user', true ));

            /*echo '<pre>';
            print_r($listProject);
            echo '</pre>';die();*/

            $k = 3;
            $v = 3;
            $sommeVR = 0;
            $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();
            $styleArray = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => \PHPExcel_Style_Border::BORDER_THIN
                    )
                )
            );
            $phpExcelObject->getDefaultStyle()->applyFromArray($styleArray);
            $phpExcelObject->setActiveSheetIndex(0)
                ->setCellValue('A1', 'Ventilation de la charge')
                ->setCellValue('A2','Nom Projet')
                ->setCellValue('B2','Ticket')
                ->setCellValue('C2','plannifié')
                ->setCellValue('D2','consommée');

            $phpExcelObject->getActiveSheet()->mergeCells('A1:B1');
            $phpExcelObject->getActiveSheet()->mergeCells('B1:C1');
            $phpExcelObject->getActiveSheet()->mergeCells('C1:D1');

            foreach ($listProject as $project => $data_project){
                $phpExcelObject->setActiveSheetIndex(0)
                    ->setCellValue('A'.$v, $project);
                if($data_project){
                    foreach ($data_project as $ticket => $data_ticket){

                        $phpExcelObject->setActiveSheetIndex(0)
                            ->setCellValue('B'.$k, $ticket);
                        if(isset($data_ticket['Temps Estimé'])){
                            $phpExcelObject->setActiveSheetIndex(0)->setCellValue('C'.$k,$data_ticket['Temps Estimé'] / 8);
                        }
                        if(isset($data_ticket['Temps Passé réel mois'])){
                            $phpExcelObject->setActiveSheetIndex(0) ->setCellValue('D'.$k,$data_ticket['Temps Passé réel mois'] / 8);
                        }

                        $k = $k + 1;
                        $v = $v + 1;
                    }

                }else{
                    $v++;
                }


            }


            $f = $k +2 ;

            $phpExcelObject->setActiveSheetIndex(0)
                ->setCellValue('B'.$f , 'Calcul des Taux');


            $style = $phpExcelObject->getActiveSheet()->getStyle('B'.$f);
            $styleFont = $style->getFont();
            $styleFont->setBold(true);
            $styleFont->getColor()->setARGB(\PHPExcel_Style_Color::COLOR_WHITE);
            $styleFont->setSize(10);
            $styleFont->setName('Arial');
            $style->applyFromArray(
                array('fill' => array('type' => \PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => '32CD32'))
                ));
            $d = $f+1;
            $phpExcelObject->setActiveSheetIndex(0)
                ->setCellValue('A'.$d, 'Velocité (hj)')
                ->setCellValue('B'.$d,'Velocité Réelle (hj)')
                ->setCellValue('C'.$d,'Velocité consommée (hj)')
                ->setCellValue('D'.$d,'% Imputation')
                ->setCellValue('E'.$d,'% Respect Date début')
                ->setCellValue('F'.$d,'% Respect Date Fin')
                ->setCellValue('G'.$d,'% depassement de plan de charge plannifié')
                ->setCellValue('H'.$d,'% Respect de plan de charge')
                ->setCellValue('I'.$d,'% de productivité');

            foreach(range('A','I') as $i) {
                $style = $phpExcelObject->getActiveSheet()->getStyle($i.$d);
                $styleFont = $style->getFont();
                $styleFont->setBold(true);
                $styleFont->getColor()->setARGB(\PHPExcel_Style_Color::COLOR_WHITE);
                $styleFont->setSize(8);
                $styleFont->setName('Arial');
                $style->applyFromArray(
                    array('fill' => array('type' => \PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => '363636'))
                    ));
            }

            $r = $d + 1;
            $n = $d + 1;
            $sommeP = 0;
            $sommeC = 0;
            $sommeImput = 0;
            $sommeRDD = 0;
            $sommeRDF = 0;
            foreach ($listProject as $project => $data_project){
                $phpExcelObject->setActiveSheetIndex(0)
                    ->setCellValue('j'.$r, $project);
                $style = $phpExcelObject->getActiveSheet()->getStyle('j'.$r);
                $style->applyFromArray(
                    array('fill' => array('type' => \PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => '7FFFD4'))
                    ));
                if($data_project){
                    foreach ($data_project as $ticket => $data_ticket){
                        $vel = $data_ticket['Temps Estimé'] / 8;
                        $vel_consom = $data_ticket['Temps Passé réel mois'] / 8;
                        $respect_date_debut = $data_ticket['Respect Date Debut'];
                        $respect_date_fin = $data_ticket['Respect Date fin'];
                        $sommeP += $vel;
                        $sommeC += $vel_consom;
                        $sommeImput += $vel_consom;
                        if($vel !== 0 AND $vel_consom !== 0){
                            $sommeVR += $vel;
                        }
                        //var_dump();die();

                        if(isset($respect_date_debut)){
                            $sommeRDD += $respect_date_debut;
                        }

                        if(isset($respect_date_fin)){
                            $sommeRDF += $respect_date_fin;
                        }
                    }

                    $phpExcelObject->setActiveSheetIndex(0)
                        ->setCellValue('A'.$r, $sommeP)
                        ->setCellValue('B'.$r,$sommeVR)
                        ->setCellValue('C'.$r,$sommeC)
                        ->setCellValue('E'.$r,($sommeRDD/sizeof($data_project)) * 100 .'%')
                        ->setCellValue('F'.$r,($sommeRDF/sizeof($data_project)) * 100 .'%');
                        if($sommeP !== 0){
                            $phpExcelObject->setActiveSheetIndex(0)->setCellValue('G'.$r,round((($sommeC - $sommeP) / $sommeP ) * 100 ,0).'%' );

                        }
                    if($sommeC !== 0){
                        $phpExcelObject->setActiveSheetIndex(0)
                            ->setCellValue('I'.$r,round(($sommeVR / $sommeC) ,0).'%');
                    } else {
                        $phpExcelObject->setActiveSheetIndex(0)
                            ->setCellValue('I'.$r,'VC est null');
                    }

                    if($sommeC !== 0 AND $sommeVR < $sommeC){
                        $phpExcelObject->setActiveSheetIndex(0)
                            ->setCellValue('I'.$r,round(($sommeVR / $sommeC) * 100 ,0).'%');
                    }


                    $r = $r +1 ;
                    $sommeP = 0;
                    $sommeC = 0;
                    $sommeVR = 0;
                    $sommeRDD = 0;
                    $sommeRDF = 0;
                }
            }
            $phpExcelObject->setActiveSheetIndex(0)->setCellValue('D'.$n,round(($sommeImput / 21) * 100 ,0). '%');
            $e = $r + 2;
            $phpExcelObject->setActiveSheetIndex(0)
                ->setCellValue('B'.$e , 'Taux du respect du plan de charge');


            $style = $phpExcelObject->getActiveSheet()->getStyle('B'.$e);
            $styleFont = $style->getFont();
            $styleFont->setBold(true);
            $styleFont->getColor()->setARGB(\PHPExcel_Style_Color::COLOR_WHITE);
            $styleFont->setSize(9);
            $styleFont->setName('Arial');
            $style->applyFromArray(
                array('fill' => array('type' => \PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => '32CD32'))
                ));

            $t = $e + 2;
            $decale_line = $t + 1;
            $decale_line1 = $t + 1;
            $sommePlan = 0;
            $sommeConsom = 0;
            $sommeVelo_reelle = 0;
            $ticketPNT = 0;
            $ticketTNP = 0;

            $phpExcelObject->setActiveSheetIndex(0)
                ->setCellValue('B'.$t,'Durée total de tickets planifiés (h)')
                ->setCellValue('C'.$t,'Tickets planifiés traités (h)')
                ->setCellValue('D'.$t,'Tickets planifiés non traités (h)')
                ->setCellValue('E'.$t,'Tickets traités non planifiés (h)')
                ->setCellValue('F'.$t,'Taux du respect du plan de charge ');

            foreach(range('B','F') as $i) {
                $style = $phpExcelObject->getActiveSheet()->getStyle($i . $t);
                $styleFont = $style->getFont();
                $styleFont->setBold(true);
                $styleFont->getColor()->setARGB(\PHPExcel_Style_Color::COLOR_WHITE);
                $styleFont->setSize(8);
                $styleFont->setName('Arial');
                $style->applyFromArray(
                    array('fill' => array('type' => \PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => '363636'))
                    ));

            }

            foreach ($listProject as $project => $data_project){
                $phpExcelObject->setActiveSheetIndex(0)
                    ->setCellValue('A'.$decale_line, $project);
                $style = $phpExcelObject->getActiveSheet()->getStyle('A'.$decale_line);
                $style->applyFromArray(
                    array('fill' => array('type' => \PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => '7FFFD4'))
                    ));
                if($data_project){
                    foreach ($data_project as $ticket => $data_ticket){
                        $vel = $data_ticket['Temps Estimé'] / 8;
                        $vel_consom = $data_ticket['Temps Passé réel mois'] / 8;
                        $sommePlan += $vel;
                        if($vel !== 0 AND $vel_consom !== 0){
                            $sommeVelo_reelle += $vel;
                        }
                        if($vel !== 0 AND $vel_consom == 0){
                            $ticketPNT += $vel;
                        }

                        if($vel == 0 AND $vel_consom !== 0){
                            $ticketTNP += $vel_consom;
                        }
                        //var_dump();die();
                    }

                    $phpExcelObject->setActiveSheetIndex(0)
                        ->setCellValue('B'.$decale_line1, $sommePlan)
                        ->setCellValue('C'.$decale_line1,$sommeVelo_reelle)
                        ->setCellValue('D'.$decale_line1,$ticketPNT)
                        ->setCellValue('E'.$decale_line1,$ticketTNP)
                        ->setCellValue('F'.$decale_line1,$sommePlan + $sommeVelo_reelle);



                    $decale_line1++;
                    $sommePlan = 0;
                    $sommeVelo_reelle = 0;
                    $ticketPNT = 0; // Ticket Plannifiées non traités
                    $ticketTNP = 0; // Ticket Traitées non plannifiées
                }
                $decale_line++;

            }


            $phpExcelObject->getActiveSheet()->getColumnDimension('A')->setWidth(15);
            $phpExcelObject->getActiveSheet()->getColumnDimension('B')->setWidth(40);
            $phpExcelObject->getActiveSheet()->getColumnDimension('C')->setWidth(22);
            $phpExcelObject->getActiveSheet()->getColumnDimension('D')->setWidth(22);
            $phpExcelObject->getActiveSheet()->getColumnDimension('E')->setWidth(22);
            $phpExcelObject->getActiveSheet()->getColumnDimension('F')->setWidth(25);
            $phpExcelObject->getActiveSheet()->getColumnDimension('G')->setWidth(40);
            $phpExcelObject->getActiveSheet()->getColumnDimension('H')->setWidth(22);
            $phpExcelObject->getActiveSheet()->getColumnDimension('I')->setWidth(15);
            $phpExcelObject->getActiveSheet()->getColumnDimension('J')->setWidth(15);

            foreach(range('A','D') as $i) {
                $style = $phpExcelObject->getActiveSheet()->getStyle($i.'2');
                $styleFont = $style->getFont();
                $styleFont->setBold(true);
                $styleFont->setSize(10);
                $styleFont->setName('Arial');
                $style->applyFromArray(
                    array('fill' => array('type' => \PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => '00BFFF'))
                    ));
            }

            $styleA1 = $phpExcelObject->getActiveSheet()->getStyle('A1');
            $styleFont = $styleA1->getFont();
            $styleFont->setBold(true);
            $styleFont->setSize(10);
            $styleFont->setName('Arial');
            $styleFont->getColor()->setARGB(\PHPExcel_Style_Color::COLOR_WHITE);
            $styleA1->applyFromArray(
                array('fill' => array('type' => \PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => '32CD32'))
                ));

            $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel5');
            // create the response
            $response = $this->get('phpexcel')->createStreamedResponse($writer);
            // adding headers
            $dispositionHeader = $response->headers->makeDisposition(
                ResponseHeaderBag::DISPOSITION_ATTACHMENT,
                'reporting.xls'
            );
            $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
            $response->headers->set('Pragma', 'public');
            $response->headers->set('Cache-Control', 'maxage=1');
            $response->headers->set('Content-Disposition', $dispositionHeader);

            return $response;
            return $this->render('RedmineTestBundle:Default:index.html.twig');



        }


}