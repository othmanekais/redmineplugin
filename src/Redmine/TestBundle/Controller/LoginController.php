<?php
/**
 * Created by PhpStorm.
 * User: Othmanekais
 * Date: 09/05/2018
 * Time: 10:00
 */

namespace Redmine\TestBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use GuzzleHttp\Client as guzzle;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class LoginController extends Controller
{

    /**
     * @return Response
     * @Route("/login",name="loginpage")
     */

    public function indexction(){

        return $this->render('RedmineTestBundle:Default:login.html.twig');
    }

    /**
     * @return Response
     * @Route("/logout",name="logout")
     */
    public function logoutAction(){

      $session = $this->get('session');
      $session->invalidate();
      $session->clear();
        //$name = $request->getSession()->remove('name');
        //$pass = $request->getSession()->remove('pass');

        return $this->render('RedmineTestBundle:Default:login.html.twig');
    }


}